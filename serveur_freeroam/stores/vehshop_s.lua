RegisterServerEvent('CheckMoneyForVeh')
RegisterServerEvent('BuyForVeh')

AddEventHandler('CheckMoneyForVeh', function(name, vehicle, price)
  TriggerEvent('es:getPlayerFromId', source, function(user)
    local player = user.getIdentifier()
    local vehicle = vehicle
    local name = name
    local price = tonumber(price)
    local sourceModel = tonumber(source)
    local result = MySQL.Sync.fetchAll("SELECT * FROM user_vehicle WHERE identifier = @username",{['@username'] = player})    
    TriggerClientEvent("es_freeroam:notify", sourceModel, "CHAR_SIMEON", 1, "Simeon", false, "Garage plein!\n")
    if(result)then
      --TriggerClientEvent('vehshop:FinishMoneyCheckForVeh', source, name, vehicle, price)      
      count = 0
      for _ in pairs(result) do
        count = count + 1
      end
      if count == 6 then
        TriggerClientEvent("es_freeroam:notify", sourceModel, "CHAR_SIMEON", 1, "Simeon", false, "Garage plein!\n")
      else        
        if (tonumber(user.getMoney()) >= tonumber(price)) then
          user.removeMoney(price)          
          TriggerClientEvent('FinishMoneyCheckForVeh', sourceModel, name, vehicle, price)
          TriggerClientEvent("es_freeroam:notify", sourceModel, "CHAR_SIMEON", 1, "Simeon", false, "Bonne route!\n")
        else
          TriggerClientEvent("es_freeroam:notify", sourceModel, "CHAR_SIMEON", 1, "Simeon", false, "Fonds insuffisants!\n")
       end
      end
    else
      if (tonumber(user.getMoney()) >= tonumber(price)) then
        user.removeMoney(price)
        TriggerClientEvent('FinishMoneyCheckForVeh', sourceModel, name, vehicle, price)
        TriggerClientEvent("es_freeroam:notify", sourceModel, "CHAR_SIMEON", 1, "Simeon", false, "Bonne route!\n")
      else
          TriggerClientEvent("es_freeroam:notify", sourceModel, "CHAR_SIMEON", 1, "Simeon", false, "Fonds insuffisants!\n")
      end
    end
  end)
end)

AddEventHandler('BuyForVeh', function(name, vehicle, price, plate, primarycolor, secondarycolor, pearlescentcolor, wheelcolor)
  TriggerEvent('es:getPlayerFromId', source, function(user)
    local player = user.getIdentifier()
    local name = name
    local price = price
    local vehicle = vehicle
    local plate = plate
    local state = "Sortit"
    local primarycolor = primarycolor
    local secondarycolor = secondarycolor
    local pearlescentcolor = pearlescentcolor
    local wheelcolor = wheelcolor
    -- local executed_query = MySQL:executeQuery("INSERT INTO user_vehicle (`identifier`, `vehicle_name`, `vehicle_model`, `vehicle_price`, `vehicle_plate`, `vehicle_state`, `vehicle_colorprimary`, `vehicle_colorsecondary`, `vehicle_pearlescentcolor`, `vehicle_wheelcolor`) VALUES ('@username', '@name', '@vehicle', '@price', '@plate', '@state', '@primarycolor', '@secondarycolor', '@pearlescentcolor', '@wheelcolor')",
    -- {['@username'] = player, ['@name'] = name, ['@vehicle'] = vehicle, ['@price'] = price, ['@plate'] = plate, ['@state'] = state, ['@primarycolor'] = primarycolor, ['@secondarycolor'] = secondarycolor, ['@pearlescentcolor'] = pearlescentcolor, ['@wheelcolor'] = wheelcolor})
    -- local executed_query = MySQL:executeQuery("INSERT INTO user_vehmods set plate='"..plate.."'" )
    MySQL.Sync.execute("INSERT INTO user_vehicle (identifier, vehicle_name, vehicle_model, vehicle_price, vehicle_plate, vehicle_state, vehicle_colorprimary, vehicle_colorsecondary, vehicle_pearlescentcolor, vehicle_wheelcolor) VALUES (@username, @name, @vehicle, @price, @plate, @state, @primarycolor, @secondarycolor, @pearlescentcolor, @wheelcolor)",{['@username'] = player, ['@name'] = name, ['@vehicle'] = vehicle, ['@price'] = price, ['@plate'] = plate, ['@state'] = state, ['@primarycolor'] = primarycolor, ['@secondarycolor'] = secondarycolor, ['@pearlescentcolor'] = pearlescentcolor, ['@wheelcolor'] = wheelcolor})
    MySQL.Sync.execute("INSERT INTO user_vehmods set plate=@plate", {['@plate'] = plate})
  end)
end)


function table_print (tt, indent, done)
  done = done or {}
  indent = indent or 0
  if type(tt) == "table" then
    local sb = {}
    for key, value in pairs (tt) do
      table.insert(sb, string.rep (" ", indent)) -- indent it
      if type (value) == "table" and not done [value] then
        done [value] = true
        table.insert(sb, "{\n");
        table.insert(sb, table_print (value, indent + 2, done))
        table.insert(sb, string.rep (" ", indent)) -- indent it
        table.insert(sb, "}\n");
      elseif "number" == type(key) then
        table.insert(sb, string.format("\"%s\"\n", tostring(value)))
      else
        table.insert(sb, string.format(
            "%s = \"%s\"\n", tostring (key), tostring(value)))
       end
    end
    return table.concat(sb)
  else
    return tt .. "\n"
  end
end

function to_string( tbl )
    if  "nil"       == type( tbl ) then
        return tostring(nil)
    elseif  "table" == type( tbl ) then
        return table_print(tbl)
    elseif  "string" == type( tbl ) then
        return tbl
    else
        return tostring(tbl)
    end
end