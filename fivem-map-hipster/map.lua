vehicle_generator "airtug" { -54.26639938354492, -1679.548828125, 28.4414, heading = 228.2736053466797 }

spawnpoint 'a_m_y_hipster_01' { x = -1005.6663208008, y = -2750.5700683594, z = 13.756640434265 }
spawnpoint 'a_m_y_hipster_02' { x = -1014.8801269531, y = -2743.0576171875, z = 13.759566307068 }
spawnpoint 'a_m_y_hipster_01' { x = -1034.75390625, y = -2751.6765136719, z = 14.597817420959 }
spawnpoint 'a_m_y_hipster_02' { x = -1046.3463134766, y = -2730.357421875, z = 20.169290542603 }
spawnpoint 'a_m_y_hipster_01' { x = -1045.35546875, y = -2734.5534667969, z = 20.169290542603 }
spawnpoint 'a_m_y_hipster_02' { x = -1052.7336425781, y = -2738.5290527344, z = 14.597834587097 }
spawnpoint 'a_m_y_hipster_01' { x = -1042.7702636719, y = -2738.7434082031, z = 20.169290542603 }
spawnpoint 'a_m_y_hipster_02' { x = -1039.8208007813, y = -2735.1762695313, z = 20.169290542603 }
spawnpoint 'a_m_y_hipster_01' { x = -1053.9664306641, y = -2714.9621582031, z = 13.756637573242 }
spawnpoint 'a_m_y_hipster_02' { x = -1038.0535888672, y = -2731.3171386719, z = 20.169290542603 }
spawnpoint 'a_m_y_hipster_01' { x = -1072.1546630859, y = -2712.767578125, z = 13.756642341614 }
spawnpoint 'a_m_y_hipster_02' { x = -1034.8310546875, y = -2734.10546875, z = 20.169290542603 }
spawnpoint 'a_m_y_hipster_01' { x = -1037.2852783203, y = -2737.6945800781, z = 20.169290542603 }
spawnpoint 'a_m_y_hipster_02' { x = -1039.8854980469, y = -2745.6325683594, z = 21.359510421753 }
spawnpoint 'a_m_y_hipster_01' { x = -1043.4467773438, y = -2675.49609375, z = 13.831531524658 }
spawnpoint 'a_m_y_hipster_02' { x = -1036.5850830078, y = -2748.6350097656, z = 21.359401702881 }
spawnpoint 'a_m_y_hipster_01' { x = -1033.1890869141, y = -2744.4455566406, z = 20.169298171997 }
spawnpoint 'a_m_y_hipster_02' { x = -1028.3587646484, y = -2741.6979980469, z = 20.169290542603 }
spawnpoint 'a_m_y_hipster_01' { x = -1011.5248413086, y = -2680.4318847656, z = 13.981985092163 }
spawnpoint 'a_m_y_hipster_02' { x = -1029.9151611328, y = -2737.53515625, z = 20.169290542603 }
spawnpoint 'a_m_y_hipster_01' { x = -992.43627929688, y = -2689.8063964844, z = 13.829986572266 }
spawnpoint 'a_m_y_hipster_02' { x = -1025.3439941406, y = -2739.6071777344, z = 20.169290542603 }
spawnpoint 'a_m_y_hipster_01' { x = -1012.2143554688, y = -2746.6647949219, z = 20.169290542603 }
spawnpoint 'a_m_y_hipster_02' { x = -962.021484375, y = -2701.4111328125, z = 13.830674171448 }
spawnpoint 'a_m_y_hipster_01' { x = -1006.4743652344, y = -2749.2299804688, z = 20.172248840332 }
spawnpoint 'a_m_y_hipster_02' { x = -956.03442382813, y = -2749.3581542969, z = 13.757598876953 }
spawnpoint 'a_m_y_hipster_01' { x = -977.31274414063, y = -2759.5275878906, z = 13.756635665894 }
spawnpoint 'a_m_y_hipster_02' { x = -1000.6322021484, y = -2762.4755859375, z = 13.959939956665 }
spawnpoint 'a_m_y_hipster_01' { x = -1053.3406982422, y = -2766.7778320313, z = 4.6398024559021 }
spawnpoint 'a_m_y_hipster_02' { x = -1032.0440673828, y = -2769.9694824219, z = 4.6397972106934 }
-- spawnpoint 'a_m_y_hipster_01' { x = -1119.33, y = 4978.52, z = 186.26 }
-- spawnpoint 'a_m_y_hipster_02' { x = 2877.3, y = 5911.57, z = 369.618 }
-- spawnpoint 'a_m_y_hipster_01' { x = 2942.1, y = 5306.73, z = 101.52 }
-- spawnpoint 'a_m_y_hipster_02' { x = 2211.29, y = 5577.94, z = 53.872 }
-- spawnpoint 'a_m_y_hipster_01' { x = 1602.39, y = 6623.02, z = 15.8417 }
-- spawnpoint 'a_m_y_hipster_02' { x = 66.0113, y = 7203.58, z = 3.16 }
-- spawnpoint 'a_m_y_hipster_01' { x = -219.201, y = 6562.82, z = 10.9706 }
-- spawnpoint 'a_m_y_hipster_02' { x = -45.1562, y = 6301.64, z = 31.6114 }
-- spawnpoint 'a_m_y_hipster_01' { x = -1004.77, y = 4854.32, z = 274.606 }
-- spawnpoint 'a_m_y_hipster_02' { x = -1580.01, y = 5173.3, z = 19.5813 }
-- spawnpoint 'a_m_y_hipster_01' { x = -1467.95, y = 5416.2, z = 23.5959 }
-- spawnpoint 'a_m_y_hipster_02' { x = -2359.31, y = 3243.83, z = 92.9037 }
-- spawnpoint 'a_m_y_hipster_01' { x = -2612.96, y = 3555.03, z = 4.85649 }
-- spawnpoint 'a_m_y_hipster_02' { x = -2083.27, y = 2616.94, z = 3.08396 }
-- spawnpoint 'a_m_y_hipster_01' { x = -524.471, y = 4195, z = 193.731 }
-- spawnpoint 'a_m_y_hipster_02' { x = -840.713, y = 4183.18, z = 215.29 }
-- spawnpoint 'a_m_y_hipster_01' { x = -1576.24, y = 2103.87, z = 67.576 }
-- spawnpoint 'a_m_y_hipster_02' { x = -1634.37, y = 209.816, z = 60.6413 }
-- spawnpoint 'a_m_y_hipster_01' { x = -1495.07, y = 142.697, z = 55.6527 }
-- spawnpoint 'a_m_y_hipster_02' { x = -1715.41, y = -197.722, z = 57.698 }
-- spawnpoint 'a_m_y_hipster_01' { x = -1181.07, y = -505.544, z = 35.5661 }
-- spawnpoint 'a_m_y_hipster_02' { x = -1712.37, y = -1082.91, z = 13.0801 }
-- spawnpoint 'a_m_y_hipster_01' { x = -1352.43, y = -1542.75, z = 4.42268 }
-- spawnpoint 'a_m_y_hipster_02' { x = -1756.89, y = 427.531, z = 127.685 }
-- spawnpoint 'a_m_y_hipster_01' { x = 3060.2, y = 2113.2, z = 1.6613 }
-- spawnpoint 'a_m_y_hipster_02' { x = 501.646, y = 5604.53, z = 797.91 }
-- spawnpoint 'a_m_y_hipster_01' { x = 714.109, y = 4151.15, z = 35.7792 }
-- spawnpoint 'a_m_y_hipster_02' { x = -103.651, y = -967.93, z = 296.52 }
-- spawnpoint 'a_m_y_hipster_01' { x = -265.333, y = -2419.35, z = 122.366 }
-- spawnpoint 'a_m_y_hipster_02' { x = 1788.25, y = 3890.34, z = 34.3849 }

--