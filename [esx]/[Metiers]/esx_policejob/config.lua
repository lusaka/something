Config                        = {}
Config.DrawDistance           = 100.0
Config.MarkerType             = 1
Config.MarkerSize             = {x = 1.5, y = 1.5, z = 1.0}
Config.MarkerColor            = {r = 50, g = 50, b = 204}
Config.EnablePlayerManagement = true
Config.EnableArmoryManagement = true
Config.EnableESXIdentity = true
Config.MaxInService           = -1
Config.Locale       		      = 'en'

Config.PoliceStations = {
	
	LSPD = {

		Blip = {
			Pos   = {x = 425.130, y = -979.558, z = 30.711},
			Color = 29
		},

		AuthorizedWeapons = {
            {name = 'WEAPON_NIGHTSTICK',       price = 150},
            {name = 'WEAPON_COMBATPISTOL',     price = 3000},
            {name = 'WEAPON_ASSAULTSMG',       price = 20000},
            {name = 'WEAPON_PUMPSHOTGUN',      price = 10000},
            {name = 'WEAPON_STUNGUN',          price = 1000},
            {name = 'WEAPON_FLASHLIGHT',       price = 100},
            {name = 'WEAPON_FLAREGUN',         price = 500},
            {name = 'WEAPON_SMOKEGRENADE',     price = 1000},
        },

		AuthorizedVehicles = {
			{name = 'tribike' , label = 'Police Vélo'},
            {name = 'police', label = 'Véhicule Cadet'},
            {name = 'police2', label = 'Véhicule de patrouille'},
            {name = 'police3', label = 'Interceptor'},
            {name = 'police4', label = 'Véhicule civil'},
            {name = 'policeb', label = 'Moto'},
            {name = 'fbi2'	 , label = 'Véhicule d’inter'},
            {name = 'policet', label = 'Van de transport'},
            {name = 'sheriff2', label = 'Police Interceptor Chevrolet'},
            {name = 'riot', label = 'Van d\'intervention blindé'},
        },

        AuthorizedHelicopters = {
			{name = 'polmav' , label = 'Helicoptere de police'},
			{name = 'buzzard2', label = 'Hélicoptère lieutenant'},
 		},

		Cloakrooms = {
            {x = 452.600, y = -993.306, z = 29.750},
            {x = -448.372, y = 6007.48, z = 30.35}
        },

		Armories = {
			{x = 451.699, y = -980.356, z = 29.689}
		},

		Vehicles = {
			{
				Spawner    = {x = 454.69, y = -1017.4, z = 27.430},
				SpawnPoint = {x = 438.42, y = -1018.3, z = 27.757},
				Heading    = 90.0
			},
			{
                Spawner    = {x = -455.304, y = 6026.84, z = 30.35},
                SpawnPoint = {x = -434.335, y = 6042.96, z = 31.3405},
                Heading    = 295.50
            }
		},

		Helicopters = {
			{
				Spawner    = {x = 451.775, y = -993.936, z = 42.691},
				SpawnPoint = {x = 450.04, y = -981.14, z = 42.691},
				Heading    = 0.0
			},
            {
                Spawner    = {x = -452.358, y = 6005.66, z = 30.9},
                SpawnPoint = {x = -475.657, y = 5988.31, z = 31.3405},
                Heading    = 0.0
            }
		},

		VehicleDeleters = {
            {x = 462.74, y = -1014.4, z = 27.065},
            {x = 462.40, y = -1019.7, z = 27.104},
            {x = 450.04, y = -981.14, z = 42.691},
            {x = -479.27, y = 6028.08, z = 30.35},
            {x = -475.657, y = 5988.31, z = 30.35},
            {x = -472.323, y = 6034.91, z = 30.35}
        },

		BossActions = {
			{x = 448.417, y = -973.208, z = 29.689}
		}

	}

}