INSERT INTO addon_account (name, label, shared) VALUES ('society_risingsun', 'Rising Sun', 1);

INSERT INTO addon_account_data (account_name, money, owner) VALUES ('society_risingsun', 0, NULL);
INSERT INTO datastore (name, label, shared) VALUES ('society_risingsun', 'Rising Sun', 1);

INSERT INTO datastore_data (name, owner, data) VALUES ('society_risingsun', NULL, '{"vehicles":[{"name":"pounder","count":1}]}');

INSERT INTO jobs (name, label, isPublic) VALUES ('risingsun', 'Rising Sun', 0);

INSERT INTO job_grades (job_name, grade, name, label, salary, skin_male, skin_female) VALUES ('risingsun', 0, 'interim', 'Intérimaire', 0, '{"tshirt_1":59,"tshirt_2":1,"torso_1":1,"torso_2":11,"decals_1":0,"decals_2":0,"arms":41,"pants_1":9,"pants_2":1,"shoes_1":12,"shoes_2":0}', '{}');

-- INSERT INTO job_grades (job_name, grade, name, label, salary, skin_male, skin_female) VALUES ('risingsun', 0, 'cdd', 'CDD', 0, '{"tshirt_1":15,"torso_1":9,"torso_2":1,"arms":30,"pants_1":10,"shoes_1":12,"helmet_1":-1}');

-- INSERT INTO job_grades (job_name, grade, name, label, salary, skin_male, skin_female) VALUES ('risingsun', 0, 'cdi', 'CDI', 0, '{"tshirt_1":15,"torso_1":95,"arms":26,"pants_1":23,"shoes_1":12,"shoes_2":3,"helmet_1":-1}');

INSERT INTO job_grades (job_name, grade, name, label, salary, skin_male, skin_female) VALUES ('risingsun', 0, 'boss', 'Patron', 0, '{"tshirt_1":15,"tshirt_2":0,"torso_1":153,"torso_2":4,"decals_1":0,"decals_2":0,"arms":1,"pants_1":9,"pants_2":1,"shoes_1":12,"shoes_2":0,"helmet_1":55,"helmet_2":11}', '{}');

INSERT INTO items (name,label) VALUES ('hop','Houblon');
INSERT INTO items (name,label) VALUES ('triple_biffle','Triple Biffle');
INSERT INTO items (name,label) VALUES ('triple_biffle_pack','Pack de Trible Biffle');