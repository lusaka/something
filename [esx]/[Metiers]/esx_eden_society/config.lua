Config              = {}
Config.DrawDistance = 100.0
Config.Taxe 		= 0.1

Config.Plates = {
	cluckinbell = "CLKNB",
	velvet= "VLVT",
	defenders = "DFDR",
}

Config.Society = {}

Config.SocietyBlips = {
	cluckinbell = {
        Pos = { x = -73.660270690918, y = 6272.9643554688, z = 31.374395370483},
		Sprite = 89,
		Color = 28,
		Name = "Cluckin Bell"
    },
	velvet = { 
        Pos = {x = -1391.9102783203, y = -584.23089599609, z = 30.23282623291},
		Sprite = 93,
		Color = 8,
		Name = "Bahamamas"
    },
	defenders = {
		Pos = { x = -564.71490478516, y = 274.01208496094, z = 83.01969909668 },
		Sprite = 355,
		Color = 26,
		Name = "Defenders"
    },
}

Config.PublicZones = {}