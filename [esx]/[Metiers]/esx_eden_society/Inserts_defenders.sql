INSERT INTO addon_account (name, label, shared) VALUES ('society_defenders', 'Defenders', 1);
INSERT INTO addon_account_data (account_name, money, owner) VALUES ('society_defenders', 0, NULL);
INSERT INTO datastore (name, label, shared) VALUES ('society_defenders', 'Defenders', 1);
INSERT INTO datastore_data (name, owner, data) VALUES ('society_defenders', NULL, '{"vehicles":[{"name":"mule3","count":1}]}');
INSERT INTO jobs (name, label, isPublic) VALUES ('defenders', 'Defenders', 0);
INSERT INTO job_grades (job_name, grade, name, label, salary, skin_male, skin_female) VALUES ('defenders', 0, 'interim', 'Intérimaire', 0, '{"tshirt_1":15,"torso_1":9,"torso_2":1,"arms":30,"pants_1":8,"shoes_1":12,"helmet_1":-1}', '{"tshirt_1":15,"torso_1":9,"torso_2":1,"arms":30,"pants_1":8,"shoes_1":12,"helmet_1":-1}');
INSERT INTO job_grades (job_name, grade, name, label, salary, skin_male, skin_female) VALUES ('defenders', 1, 'cdd', 'CDD', 0, '{"tshirt_1":15,"torso_1":9,"torso_2":1,"arms":30,"pants_1":10,"shoes_1":12,"helmet_1":-1}', '{"tshirt_1":15,"torso_1":9,"torso_2":1,"arms":30,"pants_1":10,"shoes_1":12,"helmet_1":-1}');
INSERT INTO job_grades (job_name, grade, name, label, salary, skin_male, skin_female) VALUES ('defenders', 2, 'cdi', 'CDI', 0, '{"tshirt_1":15,"torso_1":95,"arms":26,"pants_1":23,"shoes_1":12,"shoes_2":3,"helmet_1":-1}', '{"tshirt_1":15,"torso_1":95,"arms":26,"pants_1":23,"shoes_1":12,"shoes_2":3,"helmet_1":-1}');
INSERT INTO job_grades (job_name, grade, name, label, salary, skin_male, skin_female) VALUES ('defenders', 3, 'boss', 'Patron', 0, '{"tshirt_1":21,"tshirt_2":4,"torso_1":28,"arms":27,"pants_1":35,"shoes_1":21,"shoes_2":9,"helmet_1":-1}', '{"tshirt_1":21,"tshirt_2":4,"torso_1":28,"arms":27,"pants_1":35,"shoes_1":21,"shoes_2":9,"helmet_1":-1}');

INSERT INTO items (name, label, `limit`, rare, can_remove) VALUES ('malt', 'Malt', -1, 0, 1);
INSERT INTO items (name, label, `limit`, rare, can_remove) VALUES ('barrel', 'Tonneau', -1, 0, 1);
INSERT INTO items (name, label, `limit`, rare, can_remove) VALUES ('non_alcoholic_beer', 'Bière sans alcool', -1, 0, 1);