local PlayersWorking = {}
local Players = {}

ESX                = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

local function Work(source, item, society)
	local society = society
	local societyName = society.SocietyName
	local ratio  = society.Ratio
	
	SetTimeout(item[1].time, function()

		if PlayersWorking[source] == true then

			local xPlayer = ESX.GetPlayerFromId(source)

			for i=1, #item, 1 do
				local itemQtty = 0
				if item[i].name ~= "Livraison" then
					itemQtty = xPlayer.getInventoryItem(item[i].db_name).count
				end

				local requiredItemQtty = 0
				if item[1].requires ~= "nothing" then
					requiredItemQtty = xPlayer.getInventoryItem(item[1].requires).count
				end

				if item[i].name ~= "Livraison" and itemQtty >= item[i].max then
					TriggerClientEvent('esx:showNotification', source, 'Vous avez le maximum de ' .. item[i].name)
				elseif item[i].requires ~= "nothing" and requiredItemQtty <= 0 then
					TriggerClientEvent('esx:showNotification', source, "Vous n'avez plus assez de " .. item[1].requires_name .. " pour continuer cette tâche.")
				else
					if item[i].name ~= "Livraison" then
						-- Chances to drop the item
						if item[i].drop == 100 then
							xPlayer.addInventoryItem(item[i].db_name, item[i].add)
						else
							local chanceToDrop = math.random(100)
							if chanceToDrop <= item[i].drop then
								xPlayer.addInventoryItem(item[i].db_name, item[i].add)
							end
						end
					else
						local societyMoney = item[i].price * ratio
					  	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_'..societyName, function(account)
					  		account.addMoney(societyMoney)
						end)
						  TriggerEvent('esx_addonaccount:getSharedAccount', 'society_centrale', function(account)
							account.addMoney(item[i].price * Config.Taxe )
						end)
					  	local workerMoney = item[i].price * (1 - ratio)
					  	xPlayer.addMoney(workerMoney)
					end

				end
			end

			if item[1].requires ~= "nothing" then
				local itemToRemoveQtty = xPlayer.getInventoryItem(item[1].requires).count
				if itemToRemoveQtty > 0 then
					xPlayer.removeInventoryItem(item[1].requires, item[1].remove)
				end
			end
			Work(source, item, society)
		end
	end)
end
RegisterServerEvent('esx_eden_society:startWork')
AddEventHandler('esx_eden_society:startWork', function(item, society)
	local source = source
	PlayersWorking[source] = true
	Work(source, item, society)
end)

RegisterServerEvent('esx_eden_society:stopWork')
AddEventHandler('esx_eden_society:stopWork', function()
	local source = source
	PlayersWorking[source] = false
end)

RegisterServerEvent('esx_eden_society:setjob')
AddEventHandler('esx_eden_society:setjob', function(societyName,id,grade)
	local xPlayer = ESX.GetPlayerFromId(id)
	local grade = grade or 0
	xPlayer.setJob(societyName, grade)
end)

ESX.RegisterServerCallback('esx_eden_society:getVehicle', function(source, cb, society)

	local society = society

	TriggerEvent('esx_datastore:getSharedDataStore', 'society_'..society, function(store)

		local vehicles = store.get('vehicles')

		if vehicles == nil then
			vehicles = {}
		end
		cb(vehicles)

	end)

end)

RegisterServerEvent('esx_eden_society:removeVehicle')
AddEventHandler('esx_eden_society:removeVehicle', function(vehicleName, societyName)

	TriggerEvent('esx_datastore:getSharedDataStore', 'society_'..societyName, function(store)

		local vehicles = store.get('vehicles')

		if vehicles == nil then
			vehicles = {}
		end

		local foundVehicle = false

		for i=1, #vehicles, 1 do
			if vehicles[i].name == vehicleName then
				vehicles[i].count = (vehicles[i].count > 0 and vehicles[i].count - 1 or 0)
				foundVehicle = true
			end
		end

		if not foundVehicle then
			table.insert(vehicles, {
				name  = vehicleName,
				count = 0
			})
		end

		 store.set('vehicles', vehicles)

	end)

end)

ESX.RegisterServerCallback('esx_eden_society:buy', function(source, cb, amount, society)
	
	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_'..society, function(account)

		if account.money >= amount then
			account.removeMoney(amount)
			cb(true)
		else
			cb(false)
		end

	end)

end)

RegisterServerEvent('esx_eden_society:addToDataStore')
AddEventHandler('esx_eden_society:addToDataStore', function(vehicleName, society)
	local society = society
	TriggerEvent('esx_datastore:getSharedDataStore', 'society_'..society, function(store)

		local vehicles = store.get('vehicles')

		if vehicles == nil then
			vehicles = {}
		end

		local foundVehicle = false

		for i=1, #vehicles, 1 do
			if vehicles[i].name == vehicleName then
				vehicles[i].count = vehicles[i].count + 1
				foundVehicle = true
			end
		end

		if not foundVehicle then
			table.insert(vehicles, {
				name  = vehicleName,
				count = 1
			})
		end

		store.set('vehicles', vehicles)

	end)
end)

ESX.RegisterServerCallback('esx_eden_society:addVehicle', function(source, cb, vehicleName, society)
	
	local society = society
	TriggerEvent('esx_datastore:getSharedDataStore', 'society_'..society, function(store)

		local vehicles = store.get('vehicles')

		if vehicles == nil then
			vehicles = {}
		end

		local foundVehicle = false

		for i=1, #vehicles, 1 do
			if vehicles[i].name == vehicleName then
				vehicles[i].count = vehicles[i].count + 1
				foundVehicle = true
			end
		end

		if not foundVehicle then
			table.insert(vehicles, {
				name  = vehicleName,
				count = 1
			})
		end

		 store.set('vehicles', vehicles)

		 cb()

	end)

end)

function table_print (tt, indent, done)
  done = done or {}
  indent = indent or 0
  if type(tt) == "table" then
    local sb = {}
    for key, value in pairs (tt) do
      table.insert(sb, string.rep (" ", indent)) -- indent it
      if type (value) == "table" and not done [value] then
        done [value] = true
        table.insert(sb, "{\n");
        table.insert(sb, table_print (value, indent + 2, done))
        table.insert(sb, string.rep (" ", indent)) -- indent it
        table.insert(sb, "}\n");
      elseif "number" == type(key) then
        table.insert(sb, string.format("\"%s\"\n", tostring(value)))
      else
        table.insert(sb, string.format(
            "%s = \"%s\"\n", tostring (key), tostring(value)))
       end
    end
    return table.concat(sb)
  else
    return tt .. "\n"
  end
end

function to_string( tbl )
    if  "nil"       == type( tbl ) then
        return tostring(nil)
    elseif  "table" == type( tbl ) then
        return table_print(tbl)
    elseif  "string" == type( tbl ) then
        return tbl
    else
        return tostring(tbl)
    end
end

ESX.RegisterServerCallback('esx_eden_society:getEmployees', function(source, cb, society)

	local society = society
	local xPlayers = ESX.GetPlayers()
	local employees = {}
	
	for i=1, #xPlayers, 1 do
		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
		if xPlayer.job.name == society then
			table.insert(employees, { name = xPlayer.name, id = xPlayer.source })
		end
	end
	
	cb(employees)

end)

ESX.RegisterServerCallback('esx_eden_society:getGrades', function(source, cb, society)

	local society = society	
	local grades = MySQL.Sync.fetchAll(
		'SELECT label, grade FROM job_grades WHERE job_name = @job_name ORDER BY grade',
		{
			['@job_name'] = society
		}
	)
	cb(grades)

end)

function dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end
