Config.Society.centrale = {
	SocietyName = "centrale",
	Ratio = 0.1,
	DrawDistance = 100.0,
	BlipInfos = {
		Sprite = 67,
		Color = 5
	},
	Vehicles = {
		Truck = {
			Spawner = 1,
			Hash = "stockade",
			Trailer = "none",
			HasCaution = true,
			Price = 500
		}
	},
	Zones = {
		CloakRoom = {
			Pos   = {x = 132.4441, y = -769.4749, z = 241.1519},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = false,
			Name  = "Vestiaire",
			Type  = "cloakroom",
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour vous changer."
		},
		Office = {
			Pos   = { x = 149.1459, y =-756.3414, z = 241.1519 },
			Size  = {x = 1.5, y = 1.5, z = 1.0},
			Color = {r = 194, g = 153, b = 255},
			Marker= 1,
			Blip  = false,
			Name  = "Bureau",
			Type  = "office",
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour gérer l'argent des convois."
		},
		Billet = {
			Pos   = { x = 2456.8781, y = -396.9238, z = 91.992 },
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = true,
			Name  = "Coffre",
			Type  = "work",
			Item  = {
				{
					name   = "Billet",
					db_name= "billet",
					time   = 3000,
					max    = 20,
					add    = 1,
					remove = 1,
					requires = "nothing",
					requires_name = "Nothing",
					drop   = 100
				}
			},
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour transférer les billets."
		},

		billetMark = {
			Pos   = {x = 3615.7600, y = 3736.0454, z = 27.690},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = true,
			Name  = "Enregistrement",
			Type  = "work",
			Item  = {
				{
					name   = "Billet marqué",
					db_name= "billetMark",
					time   = 5000,
					max    = 20,
					add    = 1,
					remove = 1,
					requires = "billet",
					requires_name = "Billet",
					drop   = 100
				}
			},
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour marquer les billets."
		},

		Banderoler = {
			Pos   = {x = 619.4624, y = 2800.4580, z =40.9197},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = true,
			Name  = "Banderoler",
			Type  = "work",
			Item  = { 
				{
					name   = "Liasse de billet",
					db_name= "liassebillet",
					time   = 4000,
					max    = 100,
					add    = 5,
					remove = 1,
					requires = "billetMark",
					requires_name = "Billet marqué",
					drop   = 100
				}
			},
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour banderoler les billets."
		},

		VehicleSpawner = {
			Pos   = {x = 2460.2033,y = -384.4754,z = 91.3255},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = false,
			Name  = "Spawner véhicule de fonction",
			Type  = "vehspawner",
			Spawner = 1,
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour sortir le véhicule de fond.",
			Caution = 2000
		},

		VehicleSpawnPoint = {
			Pos   = { x = 2444.5510, y = -383.2123, z = 91.992 },
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Marker= -1,
			Blip  = false,
			Name  = "Véhicule de fonction",
			Type  = "vehspawnpt",
			Spawner = 1,
			Heading = 130.1
		},
		
		VehicleDeletePoint = {
			Pos   = { x = 2444.5510, y = -383.2123, z = 91.992 },
			Size  = {x = 5.0, y = 5.0, z = 1.0},
			Color = {r = 255, g = 0, b = 0},
			Marker= 1,
			Blip  = false,
			Name  = "Supression du véhicule",
			Type  = "vehdelete",
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour rendre le véhicule.",
			Spawner = 1,
			Caution = 2000,
			GPS = 0,
			Teleport = 0
		},

		Delivery = {
			Pos   = { x = 254.2981, y = 225.7653, z = 100.8757 },
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 1.0},
			Marker= 1,
			Blip  = true,
			Name  = "Point de livraison",
			Type  = "delivery",
			Spawner = 1,
			Item  = {
				{
					name   = "Livraison",
					time   = 500,
					remove = 1,
					max    = 100, -- if not present, probably an error at itemQtty >= item.max in esx_jobs_sv.lua
					price  = 14,
					requires = "liassebillet",
					requires_name = "Liasse de billet",
					drop   = 100
				}
			},
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour livrer les liasses de billet."
		}
	}
}