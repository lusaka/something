ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
	TriggerEvent('esx_service:activateService', 'ambulance', Config.MaxInService)
end

ESX.RegisterServerCallback("esx_ambulancejob:getAlive", function(source,cb)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local alive = MySQL.Sync.fetchScalar('SELECT alive FROM `users` WHERE `identifier` = @identifier',{['@identifier'] = xPlayer.getIdentifier()}	)
	cb(alive)
end)

RegisterServerEvent('esx_ambulancejob:setAlive')
AddEventHandler('esx_ambulancejob:setAlive', function(alive)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local alive = alive
	MySQL.Async.execute(
		'UPDATE `users` SET `alive` = @alive WHERE `identifier` = @identifier',
		{
			['@identifier'] = xPlayer.getIdentifier(),
			['@alive']      = alive
		}
	)
end)



RegisterServerEvent('esx_ambulancejob:revive')
AddEventHandler('esx_ambulancejob:revive', function(target)
	TriggerClientEvent('esx_ambulancejob:revive', target)
end)

ESX.RegisterServerCallback('esx_ambulancejob:removeItemsAfterRPDeath', function(source, cb)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)


	if Config.RemoveCashAfterRPDeath then
		if(xPlayer.getMoney() > 0) then
			xPlayer.set('money', 1)
			xPlayer.removeMoney(1)
		end
		if(xPlayer.getAccount('black_money').money > 0) then
			xPlayer.setAccountMoney('black_money', 1)
			xPlayer.removeAccountMoney('black_money', 1)
		end
	end

	if Config.RemoveItemsAfterRPDeath then
		for i=1, #xPlayer.inventory, 1 do
			if xPlayer.inventory[i].count > 0 and not xPlayer.inventory[i].rare then
				xPlayer.setInventoryItem(xPlayer.inventory[i].name, 0)
			end
		end
	end

	if Config.EarlyRespawnFine then
		
		local fine = 0

		TriggerEvent('esx_service:getInServiceCount','ambulance',function(countInService)
			if(countInService == 0) then
			  	fine = Config.NoEmsFine
			else
				fine =  Config.EarlyRespawnFineAmount
			end
		end)

		xPlayer.removeAccountMoney('bank', fine)		

		TriggerEvent('esx_addonaccount:getSharedAccount', 'society_ambulance', function(account)
	  		account.addMoney(fine)
	  	end)

	end

	cb()

end)

ESX.RegisterServerCallback('esx_ambulancejob:removeItemsAfterRPDeathRemoveMoney', function(source, cb)

	local xPlayer = ESX.GetPlayerFromId(source)

	if Config.RemoveCashAfterRPDeath then
		xPlayer.set('money', 0)
		xPlayer.setAccountMoney('black_money', 0)
	end

	-- if Config.EarlyRespawn and Config.EarlyRespawnFine then
	-- 	xPlayer.removeAccountMoney('bank', Config.EarlyRespawnFineAmount)
	-- end

	if Config.EarlyRespawnFine then
		xPlayer.removeAccountMoney('bank', Config.EarlyRespawnFineAmount)
	end

	if Config.RemoveItemsAfterRPDeath then
		for i=1, #xPlayer.inventory, 1 do
			if xPlayer.inventory[i].count > 0 and not xPlayer.inventory[i].rare then
				xPlayer.setInventoryItem(xPlayer.inventory[i].name, 0)
			end
		end
	end

	if Config.RemoveWeaponsAfterRPDeath then
		for i=1, #xPlayer.loadout, 1 do
			xPlayer.removeWeapon(xPlayer.loadout[i].name)
				
			
		end
		
	end

	cb()

end)

ESX.RegisterServerCallback('esx_ambulancejob:getBankMoney', function(source, cb)

    local xPlayer = ESX.GetPlayerFromId(source)
    local money = xPlayer.getAccount('bank').money

    cb(money)
end)

TriggerEvent('es:addGroupCommand', 'revive', 'admin', function(source, args, user)

	if args[2] ~= nil then
		TriggerClientEvent('esx_ambulancejob:revive', tonumber(args[2]))
	else
		TriggerClientEvent('esx_ambulancejob:revive', source)
	end

end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Insufficient Permissions.")
end, {help = _U('revive_help'), params = {{name = 'id'}}})

RegisterServerEvent('eden_ambulance:removeweaponquimarche')
AddEventHandler('eden_ambulance:removeweaponquimarche', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	
	for i=1, #xPlayer.loadout, 1 do
		xPlayer.removeWeapon(xPlayer.loadout[i].name)
	end

end)	