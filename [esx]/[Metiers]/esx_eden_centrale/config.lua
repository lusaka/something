Config                        = {}
Config.DrawDistance           = 100.0
Config.MarkerType             = 1
Config.MarkerSize             = {x = 1.5, y = 1.5, z = 1.0}
Config.MarkerColor            = {r = 50, g = 50, b = 204}
Config.EnablePlayerManagement = true
Config.EnableArmoryManagement = true
Config.MaxInService           = -1
Config.Locale       		  = 'fr'

Config.centrale = {
	
	Centrale = {

		Blip = {
			Pos   = { x = 2507.5346, y = -384.1743, z = 93.1199 },
			Color = 29
		},

		AuthorizedWeapons = {
            {name = 'WEAPON_NIGHTSTICK',       price = 150},
            {name = 'WEAPON_COMBATPISTOL',     price = 3000},
            {name = 'WEAPON_STUNGUN',          price = 1000},
            {name = 'WEAPON_FLASHLIGHT',       price = 100},
            {name = 'WEAPON_FLAREGUN',         price = 500},
            {name = 'WEAPON_SMOKEGRENADE',     price = 1000},
        },

		AuthorizedVehicles = {
			{name = 'felon', label = 'Vehicule de déplacement'},
			{name = 'stretch' , label = 'Limousine'},
            {name = 'schafter5', label = 'Berline Blindée V12'},
            {name = 'xls2', label = 'SUV blindé'},
        },

        AuthorizedHelicopters = {
			{name = 'swift' , label = 'Helicoptere de déplacement'},
			{name = 'buzzard2', label = 'Hélicoptère de reco'},
 		},

		Cloakrooms = {
			{x = 124.4695, y = -747.650, z = 241.152}
		},

		Armories = {
			{x = 118.847, y = -728.951, z = 241.151}
		},

		Vehicles = {
			{
				Spawner    = { x = 2495.443,y = -307.866, z = 92.2257},
				SpawnPoint = { x = 2501.7180, y = -309.5653, z = 91.9928 },
				Heading    = 180.0
			}
		},

		Helicopters = {
			{
				Spawner    = {x = 2505.889, y = -346.097, z = 117.025},
				SpawnPoint = {x = 2510.335, y = -341.664, z = 117.186},
				Heading    = 0.0
			}
		},

		VehicleDeleters = {
			{ x = 2501.7180, y = -309.5653, z = 91.9928 },
			{x = 2510.335, y = -341.664, z = 117.186},
		},

		BossActions = {
			{x = 109.509, y = -744.996, z = 241.152}
		}

	}

}