# fxserver-eden_darknet

FXServer ESX Eden Darknet

[REQUIREMENTS]


[INSTALLATION]

1) CD in your resources/[esx] folder
2) Clone the repository
```
git clone https://github.com/ESX-PUBLIC/eden_darknet eden_darknet
```
3) * Darknet item : Import eden_darknet.sql in your database

4) Add this in your server.cfg :

```
start eden_darknet
```
5) You need to put an item seller to permite people buy it. I will release soon an item seller.

