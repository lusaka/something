Config.Jobs.fisherman = {
	BlipInfos = {
		Sprite = 68,
		Color = 38
	},
	Vehicles = {
		Truck = {
			Spawner = 1,
			Hash = "benson",
			Trailer = "none",
			HasCaution = true
		},
		Boat = {
			Spawner = 2,
			Hash = "tug",
			Trailer = "none",
			HasCaution = false
		}
	},
	Zones = {
		CloakRoom = {
			Pos   = {x = -1593.0599, y = 5203.1684, z = 3.3100},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = true,
			Name  = _U('fm_fish_locker'),
			Type  = "cloakroom",
			Hint  = _U('cloak_change'),
			GPS = {x = -1611.6176, y = 5261.0634, z = 2.9977}
		},

		FishingSpot = {
			Pos   = {x = -2218.6723, y = 5826.0317, z = 0.0},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 110.0, y = 110.0, z = 10.0},
			Marker= 1,
			Blip  = true,
			Name  = _U('fm_fish_area'),
			Type  = "work",
			Item  = {
				{
					name   = _U('fm_fish'),
					db_name= "fish",
					time   = 2000,
					max    = 100,
					add    = 1,
					remove = 1,
					requires = "nothing",
					requires_name = "Nothing",
					drop   = 100
				}
			},
			Hint  = _U('fm_fish_button'),
			GPS = {x = -1600.34326, y = 5188.8808, z = 3.3100}
		},

		ViderLePoisson = { 
			Pos   = {x = -1600.34326, y = 5188.8808, z = 3.3100},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = true,
			Name  = _U('fm_empty_and_clean_the_fish'),
			Type  = "work",
			Item  = {
				{
					name   = _U('fm_empty_and_clean_the_fish'),
					db_name= "cleanFish",
					time   = 2000,
					max    = 100,
					add    = 1,
					remove = 1,
					requires = "fish",
					requires_name = _U('fm_fish'),
					drop   = 100
				}
			},
			Hint  = _U('fm_clean_fish_button'),
			GPS = {x = -2963.5510, y = 432.2771, z = 14.276}
		},

		CuirePoisson = { 
			Pos   = {x = -2963.5510, y = 432.2771, z = 14.276},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = true,
			Name  = _U('fm_cook_fish'),
			Type  = "work",
			Item  = {
				{
					name   = _U('fm_cocked_fish'),
					db_name= "cookedfish",
					time   = 2000,
					max    = 50,
					add    = 1,
					remove = 2,
					requires = "cleanFish",
					requires_name = _U('fm_empty_and_clean_the_fish'),
					drop   = 100
				}
			},
			Hint  = _U('fm_cook_fish_button'),
			GPS = {x = -1661.5566, y = -535.16424, z = 34.303981}
		},

		BoatSpawner = {
			Pos   = {x = -1611.6176, y = 5261.0634, z = 2.9977},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = true,
			Name  = _U('fm_spawnboat_title'),
			Type  = "vehspawner",
			Spawner = 2,
			Hint  = _U('fm_spawnboat'),
			Caution = 0,
			GPS = {x = -2218.6723, y = 5826.0317, z = 0.0}
		},

		BoatSpawnPoint = {
			Pos   = {x = -1593.7432, y = 5259.0834, z = 0.0},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Marker= -1,
			Blip  = false,
			Name  = _U('fm_boat_title'),
			Type  = "vehspawnpt",
			Spawner = 2,
			GPS = 0,
			Heading = 260
		},

		BoatDeletePoint = {
			Pos   = {x = -1620.6777, y = 5249.212, z = 0.0},
			Size  = {x = 10.0, y = 10.0, z = 1.0},
			Color = {r = 255, g = 0, b = 0},
			Marker= 1,
			Blip  = false,
			Name  = _U('fm_boat_return_title'),
			Type  = "vehdelete",
			Hint  = _U('fm_boat_return_button'),
			Spawner = 2,
			Caution = 0,
			GPS = {x = -1012.64758300781, y = -1354.62634277344, z = 5.54292726516724},
			Teleport = {x = -1611.6176, y = 5261.0634, z = 2.9977}
		},


		VehicleSpawner = {
	       	Pos   = {x = -1574.5588, y = 5173.5229, z = 18.546},
	        Size  = {x = 3.0, y = 3.0, z = 1.0},
	        Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = false,
			Name  = _U('spawn_veh'),
			Type  = "vehspawner",
			Spawner = 1,
			Hint  = _U('spawn_veh_button'),
			Caution = 500,
			GPS = {x = 3867.44, y = 4463.62, z = 1.72386}
		},

		VehicleSpawnPoint = {
	        Pos   = {x = -1578.0949, y = 5163.5849, z = 18.588},
	        Size  = {x = 3.0, y = 3.0, z = 1.0},
	        Color = {r = 232, g = 128, b = 25},
			Marker= 1,
			Blip  = false,
			Name  = _U('service_vh'),
			Type  = "vehspawnpt",
			Spawner = 1,
			GPS = 0,
			Heading = 160
		},

		VehicleDeletePoint = {
	        Pos   = {x = -1585.0447, y = 5155.819, z = 18.6321},
	        Size  = {x = 5.0, y = 5.0, z = 1.0},
	        Color = {r = 255, g = 0, b = 0},
			Marker= 1,
			Blip  = false,
			Name  = _U('return_vh'),
			Type  = "vehdelete",
			Hint  = _U('return_vh_button'),
			Spawner = 1,
			Caution = 500,
			GPS = 0,
			Teleport = 0
		},

		Delivery = {
			Pos   = {x = -1661.5566, y = -535.16424, z = 34.303981},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = true,
			Name  = _U('delivery_point'),
			Type  = "delivery",
			Spawner = 2,
			Item  = {
				{
					name   = _U('delivery'),
					time   = 500,
					remove = 1,
					max    = 50, -- if not present, probably an error at itemQtty >= item.max in esx_jobs_sv.lua
					price  = 22,
					requires = "cookedfish",
					requires_name = _U('fm_cocked_fish'),
					drop   = 100
				}
			},
			Hint  = _U('fm_deliver_fish'),
			GPS = {x = -1611.6176, y = 5261.0634, z = 2.9977}
		}
	}
}
