ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('esx_jobs:getItem')
AddEventHandler('esx_jobs:getItem', function(type, itemName, count, society)

  local _source      = source
  local xPlayer      = ESX.GetPlayerFromId(_source)
  local society		 = society

  if type == 'item_standard' then
  
    TriggerEvent('esx_addoninventory:getSharedInventory', 'society_' .. society, function(inventory)

		local item = inventory.getItem(itemName)
		
		if item.count >= count then
			inventory.removeItem(itemName, count)
			xPlayer.addInventoryItem(itemName, count)
			
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_removed') .. count .. ' ' .. item.label)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('amount_invalid'))
		end

    end)

  end

  if type == 'item_account' then

  local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_' .. society .. '_black_money', function(account)

		if count > 0 and account.money >= count then

			account.removeMoney(count)
			xPlayer.addAccountMoney(itemName, count)

			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_removed') .. count .. _U('of_dirty_money'))

		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('amount_invalid'))
		end

	end)

  end

end)

ESX.RegisterServerCallback('esx_jobs:getPlayerInventory', function(source, cb)

  local xPlayer    = ESX.GetPlayerFromId(source)
  local blackMoney = xPlayer.getAccount('black_money').money
  local items      = xPlayer.inventory

  cb({
    blackMoney = blackMoney,
    items      = items
  })

end)

RegisterServerEvent('esx_jobs:putItem')
AddEventHandler('esx_jobs:putItem', function(type, item, count, society)
  
  local _source      = source
  local xPlayer      = ESX.GetPlayerFromId(_source)

  if type == 'item_standard' then
	
	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_' .. society, function(inventory)
		
		local items = inventory.getItem(item)
		local playerItemCount = xPlayer.getInventoryItem(item).count
		
		if playerItemCount >= count then
			xPlayer.removeInventoryItem(item, count)
			inventory.addItem(item, count)
			
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_added') .. count .. ' ' .. items.label)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('amount_invalid'))
		end
		
	end)

  end

  if type == 'item_account' then

	if count > 0 and xPlayer.getAccount('black_money').money >= count then

		TriggerEvent('esx_addonaccount:getSharedAccount', 'society_' .. society .. '_black_money', function(account)
			xPlayer.removeAccountMoney(item, count)
			account.addMoney(count)
		end)

		TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_added') .. count .. _U('of_dirty_money'))

	else
		TriggerClientEvent('esx:showNotification', xPlayer.source, _U('amount_invalid'))
	end

  end

end)

ESX.RegisterServerCallback('esx_jobs:putWeapon', function(source, cb, weaponName, society)
	
	local xPlayer = ESX.GetPlayerFromId(source)

	xPlayer.removeWeapon(weaponName)

	TriggerEvent('esx_datastore:getSharedDataStore', 'society_'  .. society, function(store)

		local weapons = store.get('weapons')

		if weapons == nil then
			weapons = {}
		end

		local foundWeapon = false

		for i=1, #weapons, 1 do
			if weapons[i].name == weaponName then
				weapons[i].count = weapons[i].count + 1
				foundWeapon = true
			end
		end

		if not foundWeapon then
			table.insert(weapons, {
				name  = weaponName,
				count = 1
			})
		end

		 store.set('weapons', weapons)

		 cb()

	end)

end)

ESX.RegisterServerCallback('esx_jobs:getSocietyWeapons', function(source, cb, society)

	TriggerEvent('esx_datastore:getSharedDataStore', 'society_' .. society, function(store)

		local weapons = store.get('weapons')

		if weapons == nil then
			weapons = {}
		end

		cb(weapons)

	end)

end)

ESX.RegisterServerCallback('esx_jobs:getWeapon', function(source, cb, weaponName, society)
	
	local xPlayer = ESX.GetPlayerFromId(source)

	xPlayer.addWeapon(weaponName, 1000)

	TriggerEvent('esx_datastore:getSharedDataStore', 'society_' .. society, function(store)

		local weapons = store.get('weapons')

		if weapons == nil then
			weapons = {}
		end

		local foundWeapon = false

		for i=1, #weapons, 1 do
			if weapons[i].name == weaponName then
				weapons[i].count = (weapons[i].count > 0 and weapons[i].count - 1 or 0)
				foundWeapon = true
			end
		end

		if not foundWeapon then
			table.insert(weapons, {
				name  = weaponName,
				count = 0
			})
		end

		 store.set('weapons', weapons)

		 cb()

	end)

end)