local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57, 
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177, 
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70, 
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX                             = nil
local PlayerData                = {}
local GUI                       = {}
GUI.Time                        = 0
local HasAlreadyEnteredMarker   = false
local LastZone                  = nil
local CurrentAction             = nil
local CurrentActionMsg          = ''
local CurrentActionData         = {}
local OnJob                     = false
local TargetCoords              = nil



Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	PlayerData.job = job
end)

function OpenartisanActionsMenu()

	local elements = {
		{label = 'Sortir un véhicule', value = 'vehicle_list'},
		{label = 'Tenue de travail', value = 'cloakroom'},
		{label = 'Tenue civile', value = 'cloakroom2'},
	}
	
	if PlayerData.job.grade_name == 'boss' then
		table.insert(elements, {label = 'Retirer du coffre entreprise', value = 'society_inventory'})
		table.insert(elements, {label = 'Deposer dans le coffre entreprise', value = 'player_inventory'})
	end
	
	if Config.EnablePlayerManagement and PlayerData.job ~= nil and PlayerData.job.grade_name == 'boss' then
  		table.insert(elements, {label = 'Retirer argent société', value = 'withdraw_society_money'})
  		table.insert(elements, {label = 'Déposer argent ',        value = 'deposit_money'})
  		table.insert(elements, {label = 'Blanchir argent',        value = 'wash_money'})
		table.insert(elements, {label = 'Recruter un employé', value = 'employee_recruit'})
		table.insert(elements, {label = 'Gestion des employés', value = 'employee_management'})
	end

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'artisan_actions',
		{
			title    = 'artisan',
			elements = elements
		},
		function(data, menu)
			if data.current.value == 'vehicle_list' then
				local elements = {
					{label = 'Van de service', value = 'burrito3'},
					{label = 'Tout-terrain de service', value = 'mesa'},				
				}

				ESX.UI.Menu.CloseAll()

				ESX.UI.Menu.Open(
					'default', GetCurrentResourceName(), 'spawn_vehicle',
					{
						title    = 'Véhicule de service',
						elements = elements
					},
					function(data, menu)
						for i=1, #elements, 1 do							
							if Config.MaxInService == -1 then
								local playerPed = GetPlayerPed(-1)
								local coords    = Config.Zones.VehicleSpawnPoint.Pos
								ESX.Game.SpawnVehicle(data.current.value, coords, 90.0, function(vehicle)
									TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
								end)
								break
							else
								ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)
									if canTakeService then
										local playerPed = GetPlayerPed(-1)
										local coords    = Config.Zones.VehicleSpawnPoint.Pos
										ESX.Game.SpawnVehicle(data.current.value, coords, 90.0, function(vehicle)
											TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
										end)
									else
										ESX.ShowNotification('Service complet : ' .. inServiceCount .. '/' .. maxInService)
									end
								end, 'artisan')
								break
							end
						end						
						menu.close()
					end,
					function(data, menu)
						menu.close()
						OpenartisanActionsMenu()
					end
				)
			end

			if data.current.value == 'cloakroom' then
				menu.close()
				ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
										local playerPed = GetPlayerPed(-1)
					
    				if skin.sex == 0 then
						SetPedComponentVariation(GetPlayerPed(-1), 8, 15, 0, 0)--Teeshrit
						SetPedComponentVariation(GetPlayerPed(-1), 11, 41, 0, 0)--Veste
						SetPedComponentVariation(GetPlayerPed(-1), 3, 53, 0, 0)--Bras
						SetPedComponentVariation(GetPlayerPed(-1), 4, 9, 9, 0)--Pantalon
						SetPedComponentVariation(GetPlayerPed(-1), 6, 24, 0, 0)--Chaussures

					else
						SetPedComponentVariation(GetPlayerPed(-1), 8, 2, 0, 0)--Teeshrit
						SetPedComponentVariation(GetPlayerPed(-1), 11, 6, 11, 0)--Veste
						SetPedComponentVariation(GetPlayerPed(-1), 3, 59, 0, 0)--Bras
						SetPedComponentVariation(GetPlayerPed(-1), 4, 30, 2, 0)--Pantalon
						SetPedComponentVariation(GetPlayerPed(-1), 6, 26, 0, 0)--Chaussures
    				end
    
				end)
			end

			if data.current.value == 'cloakroom2' then
				menu.close()
				ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)

    				TriggerEvent('skinchanger:loadSkin', skin)
    
				end)
			end
			
			if data.current.value == 'society_inventory' then
				OpenRoomInventoryMenu()
			end

			if data.current.value == 'player_inventory' then
				OpenPlayerInventoryMenu()
			end

			if data.current.value == 'withdraw_society_money' then
				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'withdraw_society_money_amount',
					{
						title = 'Montant du retrait'
					},
					function(data, menu)
						local amount = tonumber(data.value)
						if amount == nil then
							ESX.ShowNotification('Montant invalide')
						else
							menu.close()
							TriggerServerEvent('esx_society:withdrawMoney', 'artisan', amount)
						end
					end,
					function(data, menu)
						menu.close()
					end
				)
			end

			if data.current.value == 'deposit_money' then
				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'deposit_money_amount',
					{
						title = 'Montant du dépôt'
					},
					function(data, menu)
						local amount = tonumber(data.value)
						if amount == nil then
							ESX.ShowNotification('Montant invalide')
						else
							menu.close()
							TriggerServerEvent('esx_society:depositMoney', 'artisan', amount)
						end
					end,
					function(data, menu)
						menu.close()
					end
				)
			end

			if data.current.value == 'wash_money' then
				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'wash_money_amount',
					{
						title = 'Montant à blanchir'
					},
					function(data, menu)
						local amount = tonumber(data.value)
						if amount == nil then
							ESX.ShowNotification('Montant invalide')
						else
							menu.close()
							TriggerServerEvent('esx_society:washMoney', 'artisan', amount)
						end
					end,
					function(data, menu)
						menu.close()
					end
				)
			end

			local societyName = 'artisan'

			if data.current.value == 'employee_recruit' then
				local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
				if closestDistance == -1 or closestDistance > 3.0 then
					ESX.ShowNotification('Aucune personne n\'est près de vous !')
				else
					local recrue = GetPlayerServerId(closestPlayer)
					TriggerServerEvent('esx_eden_society:setjob', societyName, recrue)
					ESX.ShowNotification('Vous avez ~b~embauché~s~ ')
				end
			end

			if data.current.value == 'employee_management' then
				menu.close()
				TriggerEvent('esx_eden_society:OpenEmployeesManagement', societyName)
			end
		end,
		function(data, menu)
			menu.close()
			CurrentAction     = 'artisan_actions_menu'
			CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour accéder au menu.'
			CurrentActionData = {}
		end
	)
end

function OpenRoomInventoryMenu()

  local societyName = 'society_artisan'
  local societyJobName = 'artisan'
  local societyBlackMoneyName = 'society_artisan_black_money'
  
  local blackMoney = 0
  local items = {}
  local weapons = {}
  local elements = {}
  
  ESX.TriggerServerCallback('esx_society:getAccountMoney', function(money)
  
    blackMoney = money
	
	table.insert(elements, {label = 'Argent sale ' .. blackMoney .. '$', type = 'item_account', value = 'black_money'})
  end, societyBlackMoneyName)
	
  ESX.TriggerServerCallback('esx_society:getAccountItems', function(items)

    print(json.encode(items))

    for i=1, #items, 1 do
	  if items[i].count > 0 then
        table.insert(elements, {label = items[i].label .. ' x' .. items[i].count, type = 'item_standard', value = items[i].name})
      end
	end
	
	ESX.TriggerServerCallback('esx_jobs:getSocietyWeapons', function(weapons)

	for i=1, #weapons, 1 do
		if weapons[i].count > 0 then
			table.insert(elements, {label = ESX.GetWeaponLabel(weapons[i].name) .. ' x' .. weapons[i].count, type = 'item_weapon', value = weapons[i].name})
		end
	end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocksItems_menu',
      {
        title    = 'inventaire entreprise',
		align    = 'top-left',
        elements = elements
      },
      function(data, menu)
	  
		if data.current.type == 'item_weapon' then
		
			menu.close()

			ESX.TriggerServerCallback('esx_jobs:getWeapon', function()
				OpenRoomInventoryMenu()
			end, data.current.value, societyJobName)
			
			ESX.SetTimeout(300, function()
				OpenRoomInventoryMenu()
			end)
		
		else

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = 'quantité ?'
          },
          function(data2, menu)

            local count = tonumber(data2.value)
			
			menu.close()

            if count == nil then
              ESX.ShowNotification('montant invalide')
            else
              TriggerServerEvent('esx_jobs:getItem', data.current.type, data.current.value, count, societyJobName)
			  
              OpenRoomInventoryMenu()
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )
		
		end

      end,
      function(data, menu)
        menu.close()
      end
	 
    )
	
	end, societyJobName)

  end, societyName)

end

function OpenPlayerInventoryMenu()

  local societyJobName = 'artisan'

  ESX.TriggerServerCallback('esx_jobs:getPlayerInventory', function(inventory)

    local elements = {}

    table.insert(elements, {label = 'Argent sale ' .. inventory.blackMoney .. '$', type = 'item_account', value = 'black_money'})

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    local playerPed  = GetPlayerPed(-1)
    local weaponList = ESX.GetWeaponList()

    for i=1, #weaponList, 1 do

      local weaponHash = GetHashKey(weaponList[i].name)

      if HasPedGotWeapon(playerPed,  weaponHash,  false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
        local ammo = GetAmmoInPedWeapon(playerPed, weaponHash)
        table.insert(elements, {label = weaponList[i].label, type = 'item_weapon', value = weaponList[i].name, ammo = ammo})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'player_inventory',
      {
        title    = 'inventaire personnel',
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        if data.current.type == 'item_weapon' then

          menu.close()

          ESX.TriggerServerCallback('esx_jobs:putWeapon', function()
			  OpenPlayerInventoryMenu()
		  end, data.current.value, societyJobName)

          ESX.SetTimeout(300, function()
            OpenPlayerInventoryMenu()
          end)

        else

          ESX.UI.Menu.Open(
            'dialog', GetCurrentResourceName(), 'put_item_count',
            {
              title = 'quantité ?',
            },
            function(data2, menu)

              menu.close()

              TriggerServerEvent('esx_jobs:putItem', data.current.type, data.current.value, tonumber(data2.value), societyJobName)

              ESX.SetTimeout(300, function()
                OpenPlayerInventoryMenu()
              end)

            end,
            function(data2,menu)
              menu.close()
            end
          )

        end

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end




function OpenartisanCraftMenu()
	if Config.EnablePlayerManagement and PlayerData.job ~= nil and PlayerData.job.grade_name ~= 'recrue' then

		local elements = {
			{label = '<span style="color:red;"> Base pour la création d\'objets </span>', value = 'bcreate'},
			{label = 'Cape d\'invisibilité', value = 'cape'},
			{label = 'Leurre explosif', value = 'leurre'},
			{label = 'Visière Neornithe', value = 'vision'},

		}

		ESX.UI.Menu.CloseAll()

		ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'artisan_craft',
			{
				title    = 'Etabli',
				elements = elements
			},
			function(data, menu)
		

						
				if data.current.value == 'bcreate' then
					menu.close()
					TriggerServerEvent('esx_artisanjob:startCraft')		
				end

				if data.current.value == 'cape' then				
					menu.close()
					TriggerServerEvent('esx_artisanjob:startCraft2')
				end

				if data.current.value == 'leurre' then
					menu.close()
					TriggerServerEvent('esx_artisanjob:startCraft3')
				end
				if data.current.value == 'vision' then
					menu.close()
					TriggerServerEvent('esx_artisanjob:startCraft4')
				end

			end,
			function(data, menu)
				menu.close()
				CurrentAction     = 'artisan_craft_menu'
				CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour accéder au menu établi.'
				CurrentActionData = {}
			end
		)
	else
		ESX.ShowNotification("Vous n'êtes ~r~pas assez expérimenté~s~ pour effectuer cette action.")
	end
end

function OpenMobileartisanActionsMenu()

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'mobile_artisan_actions',
		{
			title    = 'Artisan',
			elements = {
				{label = 'Facturation',   value = 'billing'},

			}
		},
		function(data, menu)
			if data.current.value == 'billing' then
				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'billing',
					{
						title = 'Montant de la facture'
					},
					function(data, menu)
						local amount = tonumber(data.value)
						if amount == nil then
							ESX.ShowNotification('Montant invalide')
						else							
							menu.close()							
							local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
							if closestPlayer == -1 or closestDistance > 3.0 then
								ESX.ShowNotification('Aucun joueur à proximité')
							else
								TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_artisan', 'artisan', amount)
							end
						end
					end,
				function(data, menu)
					menu.close()
				end
				)
			end

			
			

		end,
	function(data, menu)
		menu.close()
	end
	)
end



RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	PlayerData.job = job
end)

AddEventHandler('esx_artisanjob:hasEnteredMarker', function(zone)
	if zone == 'artisanActions' then
		CurrentAction     = 'artisan_actions_menu'
		CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour accéder au menu.'
		CurrentActionData = {}
	end
	if zone == 'Recolte1' then
		CurrentAction     = 'artisan_harvest_menu'
		CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour accéder au menu de récolte.'
		CurrentActionData = {}
	end
	if zone == 'Recolte2' then
		CurrentAction     = 'artisan_harvest_menu_citrouilles'
		CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour accéder au menu de récolte.'
		CurrentActionData = {}
	end
	if zone == 'Recolte3' then
		CurrentAction     = 'artisan_harvest_menu_crapaud'
		CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour accéder au menu de récolte.'
		CurrentActionData = {}
	end
	if zone == 'Craft' then
		CurrentAction     = 'artisan_craft_menu'
		CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour accéder au menu établi.'
		CurrentActionData = {}
	end
	if zone == 'VehicleDeleter' then
		local playerPed = GetPlayerPed(-1)
		if IsPedInAnyVehicle(playerPed,  false) then
			CurrentAction     = 'delete_vehicle'
			CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour ranger le véhicule.'
			CurrentActionData = {}
		end
	end
end)

AddEventHandler('esx_artisanjob:hasExitedMarker', function(zone)

	if zone == 'Craft' then
		TriggerServerEvent('esx_artisanjob:stopCraft')
		TriggerServerEvent('esx_artisanjob:stopCraft2')
		TriggerServerEvent('esx_artisanjob:stopCraft3')
		TriggerServerEvent('esx_artisanjob:stopCraft4')
	end

	if zone == 'Recolte1' then
		TriggerServerEvent('esx_artisanjob:stopHarvest')
	end
	if zone == 'Recolte2' then

		TriggerServerEvent('esx_artisanjob:stopHarvest2')

	end
	if zone == 'Recolte3' then

		TriggerServerEvent('esx_artisanjob:stopHarvest3')
	end

	CurrentAction = nil
	ESX.UI.Menu.CloseAll()
end)




-- Create Blips
Citizen.CreateThread(function()		
	
local blip = AddBlipForCoord(Config.Zones.artisanActions.Pos.x, Config.Zones.artisanActions.Pos.y, Config.Zones.artisanActions.Pos.z)
	SetBlipSprite (blip, 478)
	SetBlipDisplay(blip, 4)
	SetBlipScale  (blip, 1.0)
	SetBlipColour (blip, 31)
	SetBlipAsShortRange(blip, true)
	BeginTextCommandSetBlipName("STRING")
	AddTextComponentString("Artisan")
	EndTextCommandSetBlipName(blip)
end)

-- Display markers
Citizen.CreateThread(function()
	while true do		
		Wait(0)		
		if PlayerData.job ~= nil and PlayerData.job.name == 'artisan' then

			local coords = GetEntityCoords(GetPlayerPed(-1))
			
			for k,v in pairs(Config.Zones) do
				if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
					DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
				end
			end
		end
	end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
	while true do
		Wait(0)
		if PlayerData.job ~= nil and PlayerData.job.name == 'artisan' then
			local coords      = GetEntityCoords(GetPlayerPed(-1))
			local isInMarker  = false
			local currentZone = nil
			for k,v in pairs(Config.Zones) do
				if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
					isInMarker  = true
					currentZone = k
				end
			end
			if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
				HasAlreadyEnteredMarker = true
				LastZone                = currentZone
				TriggerEvent('esx_artisanjob:hasEnteredMarker', currentZone)
			end
			if not isInMarker and HasAlreadyEnteredMarker then
				HasAlreadyEnteredMarker = false
				TriggerEvent('esx_artisanjob:hasExitedMarker', LastZone)
			end
		end
	end
end)



-- Key Controls
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if CurrentAction ~= nil then
            SetTextComponentFormat('STRING')
            AddTextComponentString(CurrentActionMsg)
            DisplayHelpTextFromStringLabel(0, 0, 1, -1)
            if IsControlJustReleased(0, 38) and PlayerData.job ~= nil and PlayerData.job.name == 'artisan' then                
                if CurrentAction == 'artisan_actions_menu' then
                    OpenartisanActionsMenu()
                end
                if CurrentAction == 'artisan_harvest_menu' then
                    TriggerServerEvent('esx_artisanjob:startHarvest')
                end
                if CurrentAction == 'artisan_harvest_menu_citrouilles' then
                    TriggerServerEvent('esx_artisanjob:startHarvest2')
                end
                if CurrentAction == 'artisan_harvest_menu_crapaud' then
                    TriggerServerEvent('esx_artisanjob:startHarvest3')
                end
                if CurrentAction == 'artisan_craft_menu' then
                    OpenartisanCraftMenu()
                end
                if CurrentAction == 'delete_vehicle' then
                    local playerPed = GetPlayerPed(-1)
                    local vehicle   = GetVehiclePedIsIn(playerPed,  false)
                    local hash      = GetEntityModel(vehicle)
                    if hash == GetHashKey('burrito3') or hash == GetHashKey('mesa') then
                        if Config.MaxInService ~= -1 then
                            TriggerServerEvent('esx_service:disableService', 'artisan')
                        end                        
                        DeleteVehicle(vehicle)
                    else
                        ESX.ShowNotification('Vous ne pouvez ranger que des ~b~véhicules d\'artisan~s~.')
                    end
                end
           
                CurrentAction = nil               
            end
        end

        if IsControlJustReleased(0, 167) and PlayerData.job ~= nil and PlayerData.job.name == 'artisan' then
            OpenMobileartisanActionsMenu()
        end
    end
end)






 


 