ESX                = nil
PlayersHarvesting  = {}
PlayersHarvesting2 = {}
PlayersHarvesting3 = {}
PlayersCrafting    = {}
PlayersCrafting2   = {}
PlayersCrafting3   = {}
PlayersCrafting4   = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
	TriggerEvent('esx_service:activateService', 'artisan', Config.MaxInService)
end

-------------- Récupération Poussière ensorcelé -------------
---- Sqlut je teste ------
local function Harvest(source)

	SetTimeout(4000, function()

		if PlayersHarvesting[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local Poussiere = xPlayer.getInventoryItem('poussiere').count

			if Poussiere >= 5 then
				TriggerClientEvent('esx:showNotification', source, '~r~Vous n\'avez plus de place')		
			else   
                xPlayer.addInventoryItem('poussiere', 1)
					
				Harvest(source)
			end
		end
	end)
end

RegisterServerEvent('esx_artisanjob:startHarvest')
AddEventHandler('esx_artisanjob:startHarvest', function()
	local _source = source
	PlayersHarvesting[_source] = true
	TriggerClientEvent('esx:showNotification', _source, "Récupération de ~b~Poussière ensorcelé~s~...")
	Harvest(source)
end)

RegisterServerEvent('esx_artisanjob:stopHarvest')
AddEventHandler('esx_artisanjob:stopHarvest', function()
	local _source = source
	TriggerClientEvent('esx:showNotification', _source, 'Vous vous êtes trop éloigné de la~b~ zone de récolte (Poussière ensorcelé)')
	PlayersHarvesting[_source] = false
end)
------------ Récupération Rune d\'assemblage --------------
local function Harvest2(source)

	SetTimeout(4000, function()

		if PlayersHarvesting2[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local assemblage  = xPlayer.getInventoryItem('assemblage').count
			if assemblage >= 5 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~plus de place')				
			else
                xPlayer.addInventoryItem('assemblage', 1)
					
				Harvest2(source)
			end
		end
	end)
end

RegisterServerEvent('esx_artisanjob:startHarvest2')
AddEventHandler('esx_artisanjob:startHarvest2', function()
	local _source = source
	PlayersHarvesting2[_source] = true
	TriggerClientEvent('esx:showNotification', _source, 'Récupération de ~b~Rune d\'assemblage~s~...')
	Harvest2(_source)
end)

RegisterServerEvent('esx_artisanjob:stopHarvest2')
AddEventHandler('esx_artisanjob:stopHarvest2', function()
	local _source = source
	TriggerClientEvent('esx:showNotification', _source, 'Vous vous êtes trop éloigné de la~b~ zone de récolte ( Rune d\'assemblage )')
	PlayersHarvesting2[_source] = false
end)
----------------- Récupération Matière première ----------------
local function Harvest3(source)

	SetTimeout(4000, function()

		if PlayersHarvesting3[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local premiere  = xPlayer.getInventoryItem('premiere').count
            if premiere >= 5 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~plus de place')					
			else
                xPlayer.addInventoryItem('premiere', 1)
					
				Harvest3(source)
			end
		end
	end)
end

RegisterServerEvent('esx_artisanjob:startHarvest3')
AddEventHandler('esx_artisanjob:startHarvest3', function()
	local _source = source
	PlayersHarvesting3[_source] = true
	TriggerClientEvent('esx:showNotification', _source, 'Récupération de ~b~Matière première~s~...')
	Harvest3(_source)
end)

RegisterServerEvent('esx_artisanjob:stopHarvest3')
AddEventHandler('esx_artisanjob:stopHarvest3', function()
	local _source = source
	TriggerClientEvent('esx:showNotification', _source, 'Vous vous êtes trop éloigné de la~b~ zone de récolte ( Matière première )')
	PlayersHarvesting3[_source] = false
end)
------------ Craft Préapration -------------------
local function Craft(source)

	SetTimeout(4000, function()

		if PlayersCrafting[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local poussiere = xPlayer.getInventoryItem('poussiere').count
			local assemblage = xPlayer.getInventoryItem('assemblage').count
			local premiere = xPlayer.getInventoryItem('premiere').count
			
			if poussiere <= 0 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ de Poussière ensorcelé')
			elseif assemblage <= 0 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ de Rune d\'assemblage')
			elseif premiere <= 0 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ de Matière première')				
			else   
                xPlayer.removeInventoryItem('poussiere', 1)
                xPlayer.removeInventoryItem('assemblage', 1)
                xPlayer.removeInventoryItem('premiere', 1)				
                xPlayer.addInventoryItem('bcreate', 1)
					
				Craft(source)
			end
		end
	end)
end

RegisterServerEvent('esx_artisanjob:startCraft')
AddEventHandler('esx_artisanjob:startCraft', function()
	local _source = source
	PlayersCrafting[_source] = true
	TriggerClientEvent('esx:showNotification', _source, 'Création d\'une base pour la ~b~fabrication d\'objet magique~s~...')
	Craft(_source)
end)

RegisterServerEvent('esx_artisanjob:stopCraft')
AddEventHandler('esx_artisanjob:stopCraft', function()
	local _source = source
	PlayersCrafting[_source] = false
end)
------------ Craft Cape --------------
local function Craft2(source)

	SetTimeout(4000, function()

		if PlayersCrafting2[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local bcreate  = xPlayer.getInventoryItem('bcreate').count
			if bcreate < 10 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ de bases')				
			else
                xPlayer.removeInventoryItem('bcreate', 10)
                xPlayer.addInventoryItem('cape', 1)
					
				Craft2(source)
			end
		end
	end)
end

RegisterServerEvent('esx_artisanjob:startCraft2')
AddEventHandler('esx_artisanjob:startCraft2', function()
	local _source = source
	PlayersCrafting2[_source] = true
	TriggerClientEvent('esx:showNotification', _source, 'Assemblage de ~b~cape d\'invisibilitée~s~...')
	Craft2(_source)
end)

RegisterServerEvent('esx_artisanjob:stopCraft2')
AddEventHandler('esx_artisanjob:stopCraft2', function()
	local _source = source
	PlayersCrafting2[_source] = false
end)
----------------- Craft leurre ----------------
local function Craft3(source)

	SetTimeout(4000, function()

		if PlayersCrafting3[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local bcreate  = xPlayer.getInventoryItem('bcreate').count
			if bcreate < 5 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ de bases')						
			else
                xPlayer.removeInventoryItem('bcreate', 5)
                xPlayer.addInventoryItem('leurre', 1)
					
				Craft3(source)
			end
		end
	end)
end

RegisterServerEvent('esx_artisanjob:startCraft3')
AddEventHandler('esx_artisanjob:startCraft3', function()
	local _source = source
	PlayersCrafting3[_source] = true
	TriggerClientEvent('esx:showNotification', _source, 'Assemblage de ~b~leurre explosif~s~...')
	Craft3(_source)
end)

RegisterServerEvent('esx_artisanjob:stopCraft3')
AddEventHandler('esx_artisanjob:stopCraft3', function()
	local _source = source
	PlayersCrafting3[_source] = false
end)


local function Craft4(source)

	SetTimeout(4000, function()

		if PlayersCrafting4[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local bcreate  = xPlayer.getInventoryItem('bcreate').count
			if bcreate < 10 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ de bases')					
			else
                xPlayer.removeInventoryItem('bcreate', 10)
                xPlayer.addInventoryItem('vision', 1)
					
				Craft4(source)
			end
		end
	end)
end

RegisterServerEvent('esx_artisanjob:startCraft4')
AddEventHandler('esx_artisanjob:startCraft4', function()
	local _source = source
	PlayersCrafting4[_source] = true
	TriggerClientEvent('esx:showNotification', _source, 'Assemblage de ~b~Visière Neornithe~s~...')
	Craft4(_source)
end)

RegisterServerEvent('esx_artisanjob:stopCraft4')
AddEventHandler('esx_artisanjob:stopCraft4', function()
	local _source = source
	PlayersCrafting4[_source] = false
end)
