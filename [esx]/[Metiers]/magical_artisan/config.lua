Config                        = {}
Config.DrawDistance           = 100.0
Config.MaxInService           = -1
Config.EnablePlayerManagement = true

Config.Zones = {

	artisanActions = {
		Pos   =  {x = 1200.63, y = -1276.875, z = 34.38},
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1
	},

	Recolte1 = {
		Pos   = {x = -534.323669433594, y = 5373.794921875, z = 69.503059387207},
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1
	},
	Recolte2 = {
		Pos   =  {x = -552.214660644531, y = 5326.90966796875, z = 72.5996017456055},
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1
	},
	Recolte3 = {
		Pos   = {x = -501.386596679688, y = 5280.53076171875, z = 79.6187744140625},
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1
	},
	Craft = {
		Pos   = {x = 1191.9681396484, y = -1261.7775878906, z = 34.170627593994},
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1
	},

	VehicleSpawnPoint = {
		Pos   = {x = 1194.6257324219, y = -1286.955078125, z = 34.121524810791},
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Type  = -1
	},

	VehicleDeleter = {
		Pos   = {x = 1216.8983154297, y = -1229.2396240234, z = 34.403507232666},
        Size  = {x = 3.0, y = 3.0, z = 1.0},
        Color = {r = 204, g = 204, b = 0},
        Type  = 1
	}

    
}
