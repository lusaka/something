USE `essentialmode`;

INSERT INTO `addon_account` (name, label, shared) VALUES
  ('society_artisan','Artisan',1)
;

INSERT INTO `jobs` (name, label) VALUES
  ('artisan','Artisan')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
  ('artisan',0,'recrue','Recrue',0,'{}','{}'),
  ('artisan',1,'novice','Eleve',0,'{}','{}'),
  ('artisan',2,'experimente','Commercant',0,'{}','{}'),
  ('artisan',3,'viceboss','Vice Professeur',0,'{}','{}'),
  ('artisan',4,'boss','Professeur',500,'{}','{}')
;

INSERT INTO `items` (name, label) VALUES
	('bcreate', 'Base de création'),
	('poussiere', 'Poussière ensorcelé'),
	('assemblage', 'Rune d\'assemblage'),
	('premiere', 'Matière première'),
;