ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

--[[ NOURRITURES ]]--

-- Pain
ESX.RegisterUsableItem('bread', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('bread', 1)
	TriggerClientEvent('esx_status:add', source, 'hunger', 200000)
	TriggerClientEvent('esx_basicneeds:onEat', source, '')
	TriggerClientEvent('esx:showNotification', source, _U('used_bread'))
end)

-- Poisson
ESX.RegisterUsableItem('cookedfish', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('cookedfish', 1)
	TriggerClientEvent('esx_status:add', source, 'hunger', 400000)
	TriggerClientEvent('esx_basicneeds:onEat', source, '')
	TriggerClientEvent('esx:showNotification', source, _U('used_fish'))
end)

-- Tacos
ESX.RegisterUsableItem('tacos', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('tacos', 1)
	TriggerClientEvent('esx_status:add', source, 'hunger', 300000)
	TriggerClientEvent('esx_basicneeds:onEat', source, '')
	TriggerClientEvent('esx:showNotification', source, _U('used_tacos'))
end)

-- Salade
ESX.RegisterUsableItem('salad', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('salad', 1)
	TriggerClientEvent('esx_status:add', source, 'hunger', 250000)
	TriggerClientEvent('esx_basicneeds:onEat', source, '')
	TriggerClientEvent('esx:showNotification', source, _U('used_salad'))
end)

-- Croissant
ESX.RegisterUsableItem('croissant', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('croissant', 1)
	TriggerClientEvent('esx_status:add', source, 'hunger', 150000)
	TriggerClientEvent('esx_basicneeds:onEat', source, '')
	TriggerClientEvent('esx:showNotification', source, _U('used_croissant'))
end)

-- Part de pizza
ESX.RegisterUsableItem('traceur', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('traceur', 1)
	TriggerClientEvent('esx_status:add', source, 'hunger', 200000)
	TriggerClientEvent('esx_basicneeds:onEat', source, '')
	TriggerClientEvent('esx:showNotification', source, _U('used_pizza'))
end)

-- Hamburger
ESX.RegisterUsableItem('hamburger', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('hamburger', 1)
	TriggerClientEvent('esx_status:add', source, 'hunger', 300000)
	TriggerClientEvent('esx_basicneeds:onEat', source, 'prop_cs_burger_01')
	TriggerClientEvent('esx:showNotification', source, _U('used_hamburger'))
end)

-- Poulet
ESX.RegisterUsableItem('packaged_chicken', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('packaged_chicken', 1)
	TriggerClientEvent('esx_status:add', source, 'hunger', 450000)
	TriggerClientEvent('esx_basicneeds:onEat', source, '')
	TriggerClientEvent('esx:showNotification', source, _U('used_packaged_chicken'))
end)


--[[ BOISSONS ]]--

-- Eau
ESX.RegisterUsableItem('water', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('water', 1)
	TriggerClientEvent('eden_gofast:going', source)
		TriggerClientEvent('esx:showNotification', source, _U('used_tea'))

end)

-- Thé
ESX.RegisterUsableItem('tea', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('tea', 1)
	TriggerClientEvent('esx_status:add', source, 'thirst', 175000)
	TriggerClientEvent('esx_basicneeds:onDrink', source, 'v_res_fa_pottea')
	TriggerClientEvent('esx:showNotification', source, _U('used_tea'))
end)

-- Coca Cola
ESX.RegisterUsableItem('cola', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('cola', 1)
	TriggerClientEvent('esx_status:add', source, 'thirst', 300000)
	TriggerClientEvent('esx_basicneeds:onDrink', source, 'prop_ecola_can')
	TriggerClientEvent('esx:showNotification', source, _U('used_cola'))
end)

-- Café
ESX.RegisterUsableItem('coffee', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('coffee', 1)
	TriggerClientEvent('esx_status:add', source, 'thirst', 250000)
	TriggerClientEvent('esx_basicneeds:onDrink', source, 'prop_fib_coffee')
	TriggerClientEvent('esx:showNotification', source, _U('used_coffee'))
end)

-- Bière
ESX.RegisterUsableItem('beer', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('beer', 1)
	TriggerClientEvent('esx_status:add', source, 'thirst', 200000)
	TriggerClientEvent('esx_basicneeds:onDrinkDrunk', source, 60000)
	TriggerClientEvent('esx:showNotification', source, _U('used_beer'))
end)

-- Triple Biffle
ESX.RegisterUsableItem('triple_biffle', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('triple_biffle', 1)
	TriggerClientEvent('esx_status:add', source, 'thirst', 400000)
	TriggerClientEvent('esx_basicneeds:onDrinkDrunk', source, 200000)
	TriggerClientEvent('esx:showNotification', source, _U('used_triple_biffle'))
end)

-- HoleMeister
ESX.RegisterUsableItem('holemeister', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('holemeister', 1)
	TriggerClientEvent('esx_status:add', source, 'thirst', 450000)
	TriggerClientEvent('esx_basicneeds:onDrinkDrunk', source, 300000)
	TriggerClientEvent('esx:showNotification', source, _U('used_holemeister'))
end)

-- FreshBeer
ESX.RegisterUsableItem('non_alcoholic_beer', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('non_alcoholic_beer', 1)
	TriggerClientEvent('esx_status:add', source, 'thirst', 400000)
	TriggerClientEvent('esx_basicneeds:onDrink', source, 'prop_amb_beer_bottle')
	TriggerClientEvent('esx:showNotification', source, _U('used_non_alcoholic_beer'))
end)

--[[ AUTRES ]]--

-- Stéroïdes
ESX.RegisterUsableItem('steroids', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('steroids', 1)
	TriggerClientEvent('esx_eden_basicneeds:onEatSteroids', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_steroid'))
end)

-- Huile pour cheveux
ESX.RegisterUsableItem('hair_oil', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('hair_oil', 1)
	TriggerClientEvent('esx_eden_basicneeds:onMoussaStuff', source, 'hair_oil')
	TriggerClientEvent('esx:showNotification', source, _U('used_hair_oil'))
end)

-- Champagne
ESX.RegisterUsableItem('champagne', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('champagne', 1)
	TriggerClientEvent('esx_eden_basicneeds:onMoussaStuff', source, 'champagne')
	TriggerClientEvent('esx:showNotification', source, _U('used_champagne'))
end)

-- Carte de Moussa
ESX.RegisterUsableItem('moussa_card', function(source)
	TriggerClientEvent('esx_eden_basicneeds:onMoussaStuff', source, 'moussa_card')
end)

-- Clope
ESX.RegisterUsableItem('clope', function(source)
  local xPlayer = ESX.GetPlayerFromId(source)
  xPlayer.removeInventoryItem('clope', 1)
  TriggerClientEvent('esx_basicneeds:onSmoke', source)
  TriggerClientEvent('esx:showNotification', source, _U('used_clope'))
end)

ESX.RegisterUsableItem('cape', function(source)
  local xPlayer = ESX.GetPlayerFromId(source)
  xPlayer.removeInventoryItem('cape', 1)
  TriggerClientEvent('esx_basicneeds:onCape', source)
end)

ESX.RegisterUsableItem('leurre', function(source)
  local xPlayer = ESX.GetPlayerFromId(source)
  xPlayer.removeInventoryItem('leurre', 1)
  TriggerClientEvent('esx_basicneeds:leurre', source)
end)

ESX.RegisterUsableItem('vision', function(source)
  local xPlayer = ESX.GetPlayerFromId(source)
  xPlayer.removeInventoryItem('vision', 1)
  TriggerClientEvent('esx_basicneeds:vision', source)
end)















AddEventHandler("EffectForAll1", function(EffectPlayer) --Triggers The Teleport Effect On Other Clients
				TriggerClientEvent("Effect1", -1, EffectPlayer)
			end)

RegisterServerEvent("EffectForAll1") --Just Don't Edit!
