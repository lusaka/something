ESX          = nil
local IsDead = false

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

AddEventHandler('esx_basicneeds:resetStatus', function()
	TriggerEvent('esx_status:set', 'hunger', 500000)
	TriggerEvent('esx_status:set', 'thirst', 500000)
end)

AddEventHandler('playerSpawned', function()

	if IsDead then
		TriggerEvent('esx_basicneeds:resetStatus')
	end

	IsDead = false
end)

AddEventHandler('esx_status:loaded', function(status)

	TriggerEvent('esx_status:registerStatus', 'hunger', 1000000, '#CFAD0F',
		function(status)
			return true
		end,
		function(status)
			status.remove(200)
		end
	)

	TriggerEvent('esx_status:registerStatus', 'thirst', 1000000, '#0C98F1',
		function(status)
			return true
		end,
		function(status)
			status.remove(250)
		end
	)

	Citizen.CreateThread(function()

		while true do
			Citizen.Wait(1000)

			local playerPed  = GetPlayerPed(-1)
			local prevHealth = GetEntityHealth(playerPed)
			local health     = prevHealth

			TriggerEvent('esx_status:getStatus', 'hunger', function(status)

				if status.val == 0 then

					if prevHealth <= 150 then
						health = health - 5
					else
						health = health - 1
					end
				end
			end)

			TriggerEvent('esx_status:getStatus', 'thirst', function(status)

				if status.val == 0 then

					if prevHealth <= 150 then
						health = health - 5
					else
						health = health - 1
					end
				end
			end)

			if health ~= prevHealth then
				SetEntityHealth(playerPed,  health)
			end
		end
	end)

	Citizen.CreateThread(function()

		while true do
			Citizen.Wait(0)

			local playerPed = GetPlayerPed(-1)

			if IsEntityDead(playerPed) and not IsDead then
				IsDead = true
			end
		end
	end)
end)

AddEventHandler('esx_basicneeds:isEating', function(cb)
	cb(IsAnimated)
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)

		if ontrace then
			Citizen.Wait(1000)

			if(timer > 0)then
				if (timer == 30 ) then
					TriggerEvent('esx:showNotification', 'Votre ~y~traceur~s~ semble devenir defectueux.')
				end
				timer = timer - 1
			else
				TriggerEvent('esx:showNotification', 'Votre ~y~traceur~s~ ne donne plus aucun signal .')
				RemoveBlip(vehBlip)
			end
		end
	end
end)

RegisterNetEvent('esx_optionalneeds:traceur')
AddEventHandler('esx_optionalneeds:traceur', function()

	local playerPed = GetPlayerPed(-1)
	local coords = GetEntityCoords(GetPlayerPed(-1))
	local veh, distance = ESX.Game.GetClosestVehicle({x = coords.x, y = coords.y, z = coords.z})

	if distance < 2 then

		Citizen.CreateThread(function()
			TaskStartScenarioInPlace(GetPlayerPed(-1), "world_human_vehicle_mechanic", 0, false)
			Wait(10000)
			ClearPedTasksImmediately(playerPed)
		end)

		Wait(10000)
		local vehicle, distance = ESX.Game.GetClosestVehicle({x = coords.x, y = coords.y, z = coords.z})

		if veh == vehicle and distance < 2 then
			vehBlip = AddBlipForEntity(veh)
			SetBlipColour(vehBlip, 1)
			timer = math.random(60, 220)
			ontrace 		= true
			TriggerEvent('esx:showNotification', 'Vous venez de poser un ~y~traceur~s~.' )
		else
			TriggerEvent('esx:showNotification', 'Il semblerais que la voiture soit partie' )
		end
	else
		TriggerEvent('esx:showNotification', 'Vous êtes trop loin du vehicule.')
	end
end)

RegisterNetEvent('esx_basicneeds:onEat')
AddEventHandler('esx_basicneeds:onEat', function(prop_name)
  if not IsAnimated then
		local playerPed = GetPlayerPed(-1)

		if prop_name ~= '' then
			local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1)))
			prop = CreateObject(GetHashKey(prop_name), x, y, z + 0.2,  true,  true, true)
			-- x = (nég) droite / (pos) gauche         y = (nég) devant / (pos) derrière
			-- test
			AttachEntityToEntity(prop, GetPlayerPed(-1), GetPedBoneIndex(GetPlayerPed(-1), 18905), 0.120, 0.010, 0.010, 5.0, 150.0, 0.0, true, true, false, true, 1, true)
		end

		IsAnimated = true

		Citizen.CreateThread(function()

			RequestAnimDict('mp_player_inteat@burger')
			while not HasAnimDictLoaded('mp_player_inteat@burger') do
				Wait(0)
			end

			TaskPlayAnim(playerPed, 'mp_player_inteat@burger', 'mp_player_int_eat_burger_fp', 8.0, -8, -1, 49, 0, 0, 0, 0)
			Wait(3000)
			IsAnimated = false
			ClearPedSecondaryTask(playerPed)
			DeleteObject(prop)
		end)
	end
end)

RegisterNetEvent('esx_basicneeds:onDrink')
AddEventHandler('esx_basicneeds:onDrink', function()

	if not IsAnimated then

		local playerPed = GetPlayerPed(-1)
		IsAnimated = true

		Citizen.CreateThread(function()

			RequestAnimDict('clothingspecs')
			while not HasAnimDictLoaded('clothingspecs') do
				Wait(0)
			end

			TaskPlayAnim(playerPed, 'clothingspecs', 'take_off', 1.0, -1.0, 2000, 0, 1, true, true, true)
			Wait(2500)
	    IsAnimated = false
	    ClearPedSecondaryTask(playerPed)
			DeleteObject(prop)
		end)
	end
end)

RegisterNetEvent('esx_basicneeds:onDrinkDrunk')
AddEventHandler('esx_basicneeds:onDrinkDrunk', function(duration)

	local pid = PlayerPedId()

	if not IsPedSittingInAnyVehicle(pid) then

		RequestAnimSet("MOVE_M@DRUNK@VERYDRUNK")
		while (not HasAnimSetLoaded("MOVE_M@DRUNK@VERYDRUNK")) do Citizen.Wait(0) end

		TaskStartScenarioInPlace(GetPlayerPed(-1), "WORLD_HUMAN_DRINKING", 0, true)
		Citizen.Wait(5000)
		DoScreenFadeOut(1000)
		Citizen.Wait(1000)
		ClearPedTasks(GetPlayerPed(-1))
		SetTimecycleModifier("spectator5")
		SetPedMotionBlur(GetPlayerPed(-1), true)
		SetPedMovementClipset(GetPlayerPed(-1), "MOVE_M@DRUNK@VERYDRUNK", -1)
		SetPedIsDrunk(GetPlayerPed(-1), true)
		DoScreenFadeIn(1000)
		SetPedConfigFlag(GetPlayerPed(-1), 100, true)
		ShakeGameplayCam("DRUNK_SHAKE", true)
		Citizen.Wait(duration)
		ShakeGameplayCam("STOP_CAM_SHAKING", true)
		SetPedConfigFlag(GetPlayerPed(-1), 100, false)
		DoScreenFadeOut(1000)
		Citizen.Wait(1000)
		DoScreenFadeIn(1000)
		ClearTimecycleModifier()
		ResetScenarioTypesEnabled()
		SetPedIsDrunk(GetPlayerPed(-1), false)
		ResetPedMovementClipset(GetPlayerPed(-1), 0) -- génère un problème de TP (même chose sur drogue) - à voir
		SetPedMotionBlur(GetPlayerPed(-1), false)
	end
end)

RegisterNetEvent('esx_eden_basicneeds:onEatSteroids')
AddEventHandler('esx_eden_basicneeds:onEatSteroids', function()
	local time = 5 * 60

	Citizen.CreateThread( function()

		while true do
		  Citizen.Wait(1000)
		  if(time > 0) then
		    ResetPlayerStamina(PlayerId())
		    time =  time - 1
		  end
		end
	end)
end)

RegisterNetEvent('esx_eden_basicneeds:onMoussaStuff')
AddEventHandler('esx_eden_basicneeds:onMoussaStuff', function(itemname)

	local pid = PlayerPedId()
	local i = 0

	if itemname == 'hair_oil' then
		local notifs = {
		'L\'~y~huile~s~ fait effet ...',
		'Vos cheveux sont toujours un peu ~y~gras~s~.',
		'Je vous promet que l\'~y~huile~s~ fait toujours effet ...',
		'Au moins l\'~y~huile~s~ sent bon ...',
		'Ça sent tout de même un peu fort votre ~y~huile~s~.' }

		Citizen.CreateThread(function()

			while (i < 5) do
				Citizen.Wait(math.random(30000,60000))
				TriggerEvent('esx:showNotification', notifs[math.random(#notifs)])
				i = i + 1
			end

			Citizen.Wait(math.random(30000,60000))
			TriggerEvent('esx:showNotification', 'L\'~y~huile~s~ a bien pénétré. Vos cheveux vont être si soyeux.')
		end)
	elseif itemname == 'champagne' then

		local notifs = {
			'Le ~b~champagne~s~ fait effet ...',
			'Le ~b~champagne~s~ fait effet ...', --increase random
			'Des ~b~bulles~s~, des ~b~bulles~s~, encore des ~b~bulles~s~...',
			'Vous vous sentez légèrement ... ~b~euphorique~s~.',
			'Le ~b~champagne~s~ vous va si bien ... une nouvelle coupe ?',
			'Vous avez des ~b~bulles~s~ plein la tête.',
			'Le ~b~champagne~s~ est excellent. Dites merci à Moussa.'
		}

		Citizen.CreateThread(function()

			TaskStartScenarioInPlace(GetPlayerPed(-1), "WORLD_HUMAN_DRINKING", 0, true)
			Citizen.Wait(5000)
			DoScreenFadeOut(1000)
			Citizen.Wait(1000)
			ClearPedTasks(GetPlayerPed(-1))
			SetTimecycleModifier("spectator5")
			SetPedMotionBlur(GetPlayerPed(-1), true)
			SetPedMovementClipset(GetPlayerPed(-1), "MOVE_M@DRUNK@MODERATEDRUNK", -1)
			SetPedIsDrunk(GetPlayerPed(-1), true)
			DoScreenFadeIn(1000)
			SetPedConfigFlag(GetPlayerPed(-1), 100, true)
			ShakeGameplayCam("DRUNK_SHAKE", true)

			while (i < 6) do
				Citizen.Wait(math.random(30000,90000))
				TriggerEvent('esx:showNotification', notifs[math.random(#notifs)])
				i = i + 1
			end

			Citizen.Wait(math.random(30000,90000))
			ShakeGameplayCam("STOP_CAM_SHAKING", true)
			SetPedConfigFlag(GetPlayerPed(-1), 100, false)
			DoScreenFadeOut(1000)
			Citizen.Wait(1000)
			DoScreenFadeIn(1000)
			ClearTimecycleModifier()
			ResetScenarioTypesEnabled()
			SetPedIsDrunk(GetPlayerPed(-1), false)
			ResetPedMovementClipset(GetPlayerPed(-1), 0)
			SetPedMotionBlur(GetPlayerPed(-1), false)
			TriggerEvent('esx:showNotification', 'L\'effet du ~y~champagne~s~ commence à s\'estomper. Attention à la migraine.')
		end)
	elseif itemname == 'moussa_card' then
		TriggerEvent('esx:showNotification', 'Numéro de ~p~Moussa~s~ : 87302')
	end
end)

RegisterNetEvent('esx_basicneeds:onSmoke')
AddEventHandler('esx_basicneeds:onSmoke', function(duration)

	local pid = PlayerPedId()
	local playerPed = GetPlayerPed(-1)

	if not IsPedSittingInAnyVehicle(pid) then
		TaskStartScenarioInPlace(GetPlayerPed(-1), "WORLD_HUMAN_SMOKING", 0, true)
	end
end)

RegisterNetEvent('esx_basicneeds:onCape')
AddEventHandler('esx_basicneeds:onCape', function(duration)

  local pid = PlayerPedId()
	onCape()
end)

RegisterNetEvent('esx_basicneeds:leurre')
AddEventHandler('esx_basicneeds:leurre', function(duration)

  local pid = PlayerPedId()
  TriggerServerEvent("EffectForAll1", PlayerId())
  onLeurre()
end)

RegisterNetEvent('esx_basicneeds:vision')
AddEventHandler('esx_basicneeds:vision', function(duration)

	local model = GetHashKey("a_c_pigeon")
	local playerPed = GetPlayerPed(-1)
	local ped = ClonePed(playerPed, GetEntityHeading(playerPed), 1, 1)
	local LastPosition = GetEntityCoords(GetPlayerPed(-1))
	ESX.ShowNotification(("La maitrise de cet objet est très difficile, si l'oiseau meurt, vous tomberez dans le coma."))

	RequestModel(model)
	while not HasModelLoaded(model) do
		RequestModel(model)
		Citizen.Wait(0)
	end

	SetPlayerModel(PlayerId(), model)
	SetModelAsNoLongerNeeded(model)

	SetTimeout(50000, function()

		ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
			local model2 = nil

			if skin.sex == 0 then
				model2 = GetHashKey("mp_m_freemode_01")
			else
				model2 = GetHashKey("mp_f_freemode_01")
			end

			RequestModel(model2)
			while not HasModelLoaded(model2) do
				RequestModel(model2)
				Citizen.Wait(1)
			end

			SetPlayerModel(PlayerId(), model2)
			SetModelAsNoLongerNeeded(model2)
			DeleteEntity(ped)
			SetEntityCoords(playerPed, LastPosition.x, LastPosition.y, LastPosition.z)
			TriggerEvent('skinchanger:loadSkin', skin)
			TriggerEvent('esx:restoreLoadout')
		end)
	end)
end)

function onCape()

	local ped = GetPlayerPed(-1)


	SetEntityVisible(ped, false, false)
	ESX.ShowNotification(("Faites attention votre cape a l\'air plûtot fragile."))

	SetTimeout(math.random(45000, 120000), function()

		SetEntityVisible(ped, true, false)
		ESX.ShowNotification(('Votre cape d\'invisibilité ne semble plus faire effet.'))
	end)
end

function onLeurre()

	local ped = GetPlayerPed(-1)
	SetEntityVisible(ped, false, false)

	SetTimeout(2000, function()

		SetEntityVisible(ped, true, false)
	end)
end


AddEventHandler("Effect1", function(EffectPlayer) --Start The Teleport Effect For Everyone Around The Player
  local Entity
  local playerPedPos = GetEntityCoords(GetPlayerPed(-1), true)
  local EffectPlayerPedPos = GetEntityCoords(GetPlayerPed(EffectPlayer), true)
  local playerDist = Vdist(playerPedPos.x, playerPedPos.y, playerPedPos.z, EffectPlayerPedPos.x, EffectPlayerPedPos.y, EffectPlayerPedPos.z)

  if playerDist < 50 then

    if IsPedInAnyVehicle(GetPlayerPed(EffectPlayer), 0) then
      Entity = GetVehiclePedIsIn(GetPlayerPed(EffectPlayer), 0)
      scale = 15.0
    else
      Entity = GetPlayerPed(EffectPlayer)
      scale = 4.0
    end

    if not HasNamedPtfxAssetLoaded("core") then
      RequestNamedPtfxAsset("core")
      while not HasNamedPtfxAssetLoaded("core") do
        Citizen.Wait(0)
      end
      if HasNamedPtfxAssetLoaded("core") then
      end
    end

    SetPtfxAssetNextCall("core")
    StartParticleFxLoopedAtCoord("exp_air_molotov", EffectPlayerPedPos.x, EffectPlayerPedPos.y, EffectPlayerPedPos.z, 0.0, 0.0, 0.0, scale, false, false, false, false)
    RemoveNamedPtfxAsset("core")
  end
end)

RegisterNetEvent("Effect1")




