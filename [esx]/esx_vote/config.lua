Config                           = {}
Config.DrawDistance              = 100.0
Config.MarkerColor               = {r = 39, g = 131, b = 206}

Config.Zones = {

	CityHall = {
		Pos  = {x = -544.99475097656, y = -204.6915435791, z = 38.216659545898 },
		Size       = {x = 1.5, y = 1.5, z = 1.0},
		Type       = 2,
		Sprite     = 409,
		Color      = 26,
		Name       = "Vote"
	}
}
