Config                           = {}
Config.DrawDistance              = 100.0
Config.MarkerColor               = {r = 102, g = 0, b = 102}

Config.Zones = {

	BahamamasEntering = {
		MarkerPos  = {x = -1388.3796386719, y = -586.57177734375, z = 29.217758178711},
		Size       = {x = 1.5, y = 1.5, z = 1.0},
		Type       = 1,
		SpawnPos   = { x = -1389.630859375, y = -590.14660644531, z = 30.31954574585 },
	},

	BahamamasExit = {
		MarkerPos  = {x = -1387.4841308594, y = -588.6884765625, z = 29.319541931152 },
		Size       = {x = 1.5, y = 1.5, z = 1.0},
		Type       = 1,
		SpawnPos   = { x = -1390.6750488281, y = -585.78173828125, z = 30.228441238403 },
	},
	
	RisingHeadOfficeEntering = {
		MarkerPos  = {x = -1581.6833496094, y = -557.80676269531, z = 34.952919006348 },
		Size       = {x = 1.5, y = 1.5, z = 1.0},
		Type       = 1,
		SpawnPos   = {x = -1579.0012207031, y = -564.34429931641, z = 108.52294921875 },
	},
	
	RisingHeadOfficeExit = {
		MarkerPos  = {x = -1580.9595947266, y = -561.10577392578, z = 108.52294921875 },
		Size       = {x = 1.5, y = 1.5, z = 1.0},
		Type       = 1,
		SpawnPos   = { x = -1583.9155273438, y = -554.72064208984, z = 34.971435546875 },
	},
	CentraleEntering = {
		MarkerPos  = {x = 2475.545, y = -384.118, z = 93.399},
		Size       = {x = 1.5, y = 1.5, z = 1.0},
		Type       = 1,
		SpawnPos   = { x = 135.3695, y = -764.184, z = 242.151 },
	},
	CentraleExit = {
		MarkerPos  = {x = 138.125, y = -764.929, z = 241.152},
		Size       = {x = 1.5, y = 1.5, z = 1.0},
		Type       = 1,
		SpawnPos   = { x = 2480.5893, y = -384.0165, z = 93.191 },
	},
	CentraleHeliEntering = {
		MarkerPos  = {x = 127.3357, y = -729.415, z = 241.151},
		Size       = {x = 1.5, y = 1.5, z = 1.0},
		Type       = 1,
		SpawnPos   = { x = 2505.836, y = -338.5171, z = 116.398 },
	},
	CentraleHeliExit = {
		MarkerPos  = {x = 2507.766, y = -336.605, z = 114.596},
		Size       = {x = 1.5, y = 1.5, z = 1.0},
		Type       = 1,
		SpawnPos   = { x = 124.527, y = -728.3656, z = 241.151 },
	},

}
