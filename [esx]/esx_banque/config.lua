Config              = {}
Config.MarkerType   = 1
Config.DrawDistance = 100.0
Config.ZoneSize     = {x = 3.0, y = 3.0, z = 0.3}
Config.MarkerColor  = {r = 100, g = 100, b = 204}
Config.ZDiff        = 2.0
Config.BlipSprite   = 500
Config.Locale       = 'fr'

Config.Banques = {
  { ['x'] = 149.4551,  ['y'] = -1038.95,  ['z'] = 29.366},
  { ['x'] = -2962.60,  ['y'] = 482.1914,  ['z'] = 15.762},
  { ['x'] = 236.4638,  ['y'] = 217.4718,  ['z'] = 106.840},
  --{ ['x'] = 265.0043,  ['y'] = 212.1717,  ['z'] = 106.780},
  { ['x'] = 314.187,   ['y'] = -278.621,  ['z'] = 54.170},
  { ['x'] = -1214.09,  ['y'] = -330.99,  ['z'] = 37.881},
  { ['x'] = -110.753,  ['y'] = 6467.703,  ['z'] = 31.784}
}
