server_scripts {
    '@es_extended/locale.lua',
    'locales/fr.lua',
    'config.lua',
    'server/esx_banque_sv.lua'
}

client_scripts {
    '@es_extended/locale.lua',
    'locales/fr.lua',
    'config.lua',
    'client/esx_banque_cl.lua'
}

ui_page 'html/ui.html'

files {
    'html/ui.html',
    'html/pdown.ttf',
    'html/img/cursor.png',
    'html/css/app.css',
    'html/scripts/app.js'
}
