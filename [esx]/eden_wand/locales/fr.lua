Locales['fr'] = {
	
	--Action Menu
		['citizen_interaction'] = 'interaction citoyen',
		['vehicle_interaction'] = 'interaction véhicule',
		['object_spawner'] = 'placer objets',

		['id_card'] = 'carte d\'identité',
		['search'] = 'fouiller',
		['handcuff'] = 'menotter / Démenotter',
		['drag'] = 'escorter',
		['put_in_vehicle'] = 'mettre dans le véhicule',
		['out_the_vehicle'] = 'sortir du véhicule',
		['drag'] = 'Enmener le joueur',
		['fine'] = 'Amende',
		['code'] = 'Retirer Permis',
		['no_players_nearby'] = 'aucun joueur à proximité',

		['vehicle_info'] = 'infos véhicule',
		['pick_lock'] = 'crocheter véhicule',
		['vehicle_unlocked'] = 'véhicule ~g~déverouillé~s~',
		['no_vehicles_nearby'] = 'aucun véhicule à proximité',
		['traffic_interaction'] = 'Interaction Voirie',
		['cone'] = 'plot',
		['barrier'] = 'barrière',
		['spikestrips'] = 'herse',
		['box'] = 'caisse',
		['cash'] = 'caisse',
	
	--Misc
		['remove_object'] = 'appuez sur ~INPUT_CONTEXT~ pour enlever l\'objet',

	
}