description 'esx_eden_chasse'

server_scripts {
    '@es_extended/locale.lua',
    'config.lua',
    'server.lua',
}

client_scripts {
    '@es_extended/locale.lua',
    'config.lua',
    'client.lua',
}