Config              = {}
Config.MarkerType   = 31
Config.DrawDistance = 3.0
Config.ZoneSize     = {x = 6.0, y = 6.0, z = 3.0}
Config.MarkerColor  = {r = 128, g = 128, b = 128}
Config.Locale = 'fr'

Config.SpawnVehicle = { x = 2007.8709, y = 4986.7729, z = 41.3601}

Config.minMeat = 2
Config.maxMeat = 9
Config.priceMeat = 20

Config.Zones = {
	TakeService = 	{ x = 2016.8183, y = 4987.885, z = 42.098209 },
	SellMeat 	=	{ x = 960.19610, y = -2105.531, z = 31.9526 },
}

-- Attention bien changer le random en fonction du tableau dans le client
Config.missionCoords = {
  {x=-1505.2, y=4887.39, z=78.38},
  {x=-1508.2, y=4795.5, z=64.36},
  {x=-1484.73, y=4668.05, z=40.143},
  {x=-1410.63, y=4730.94, z=44.0369},
  {x=-1377.29, y=4864.31, z=134.162},
  {x=-1697.63, y=4652.71, z=22.2442},
  {x=-1160.345, y=4632.1850, z=145.47000},
  {x=-974.802, y=4409.66, z=19.3078},
  {x=-1820.96, y=4691.03, z=4.4164},
  {x=-1671.7, y=5018.13, z=34.1412},
  {x=-517.922, y=4356.227, z=67.657},
  {x=-871.3680, y=4385.5732, z=20.848},
  {x=-1142.450, y=4853.1391, z=198.768},
  {x=-998.968, y=4702.1621, z=250.5881}
}
