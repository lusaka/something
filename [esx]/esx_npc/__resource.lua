description 'ESX NPCs'

client_scripts {
	'config.lua',
	'npc/weed_dealer.lua',
	'client/main.lua'
}

server_scripts {
	'server/main.lua'
}