ESX                = nil
local FirstSpawned = true
local NPCS 			= nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)


ESX.RegisterServerCallback('esx_npc:isFirstSpawn', function(source, cb)

	if FirstSpawned then
		FirstSpawned = false
		cb(true)
	else
		cb(false)
	end

end)

RegisterServerEvent('esx_npc:setNPCs')
AddEventHandler('esx_npc:setNPCs', function(NPCs)
	print('setting NPCS:' .. json.encode(NPCs))
	NPCS = NPCs
end)

RegisterServerEvent('esx_npc:getNPCs')
AddEventHandler('esx_npc:getNPCs', function()
	local _source = source
	print('asking for NPCS:' .. json.encode(NPCS))
	TriggerClientEvent('esx_npc:getNPCs', _source, NPCS)
end)

RegisterServerEvent('esx_npc:addItem')
AddEventHandler('esx_npc:addItem', function(item, qtty, price)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local price = price or nil

	print('#################### addItem: ' .. item .. qtty .. price)

	if xPlayer.getInventoryItem(item) ~= nil then
		print('#################### item ~= nil')
		if price == nil then
			print('#################### price == nil')
			xPlayer.addInventoryItem(item, qtty)
		elseif price <= xPlayer.getMoney() then
			print('#################### price < xPlayer.getMoney()')
			xPlayer.removeMoney(price)
			xPlayer.addInventoryItem(item, qtty)
		else
			print('#################### not enough cash')
			TriggerClientEvent('esx:showNotification', _source, 'Vous n\'avez pas assez de cash !')
			TriggerClientEvent('esx_npc:attackPlayer', _source)
		end
	else
		TriggerClientEvent('esx:showNotification', _source, 'This item is not in your Database')
	end

end)

RegisterServerEvent('esx_npc:removeItem')
AddEventHandler('esx_npc:removeItem', function(item, qtty, price)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local price = price or nil

	if xPlayer.getInventoryItem(item) ~= nil then
		if price == nil then		
			xPlayer.removeInventoryItem(item, qtty)
		else
			xPlayer.addMoney(price)
			xPlayer.removeInventoryItem(item, qtty)
		end
	else
		TriggerClientEvent('esx:showNotification', _source, 'This item is not in your Database')
	end
end)