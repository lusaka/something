Locales['en'] = {
	-------------------------------------
	-------------- Client ---------------
	-------------------------------------
	['time'] = 'Il vous reste ~r~',
	['time_minute'] = '~s~ minutes pour transporter la marchandise.',
	['press_start']	= 'Appuyez sur ~INPUT_CONTEXT~ pour obtenir une mission.',
	['press_finish'] = 'Appuyez sur ~INPUT_CONTEXT~ pour livrer le vehicule.',
	['not_good_veh'] = '~r~Tu n\'as pas le bon vehicule~s~, tu me prend pour un con ?!',
	['give_drug'] = 'Tient voila la marchandise. On te rendra tes $',
	['give_drug2'] = ' quand tu ramenera la caisse.',
	['give_veh'] = 'Cette fois tu auras : ~g~',
	['give_veh2'] = ' ~s~comme vehicule ',
	['finish'] = 'La prochaine fois soi plus rapide. Tout le monde descend du vehicule.',
	['finish_time'] = 'T\'es arriver en retard, rend ma caisse et casse toi.',

	-------------------------------------
	-------------- Server ---------------
	-------------------------------------

	['have_drug'] = '~r~Vous avez deja de la marchandise',
	['police_start'] = '~g~Un GoFast viens de commencer',
	['need_cop'] = 'Il n\'y as pas sufisament de policer',
	['dont_have_pack_drugs'] = 'Tu n\'as pas la drogue', 
	['police_finish'] = '~r~Le GoFast viens de finir',
}
