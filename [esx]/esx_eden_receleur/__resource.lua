description 'esx_eden_mexiacain'

server_scripts {
	"@mysql-async/lib/MySQL.lua",
	'config.lua',
    'server.lua'
}

client_scripts {	
	'config.lua',
    'client.lua'
}