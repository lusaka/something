Locales['fr'] = {


	--Title and subtitles 
	
		['demarches'] = 'Démarches',
		['categories'] = 'Categories',
		['shumeurs'] = 'Humeurs',
		['sattitudes'] = 'Attitudes',
		['sautres'] = 'Autres',
		['sfemmes'] = 'Special Femmes',
		['reset'] = '~r~Reset',
		['quitter'] = "~o~Quitter",
		
	--Main Menu
	
		['humeurs'] = '~b~Humeurs',
		['attitudes'] = '~b~Attitudes',
		['autres'] = '~b~Autres',
		['femmes'] = '~b~Spécial Femmes',
	
	-- Moods
	
		['determine'] = 'Determiné',
		['triste'] = 'Triste',
		['depression'] = 'Depression',
		['enerve'] = 'Enervé',
		['presse'] = 'Pressé',
		['timide'] = 'Timide',
		['lunatique'] = 'Lunatique',
		['stresse'] = 'Stressé',
		['flemme'] = 'Flemme',
		
	-- Attitudes
	
		['hautain'] = 'Hautain',
		['badboy'] = 'Bad Boy',
		['gangster'] = 'Gangster',
		['fraquasse'] = 'Fraquassé',
		['coquille'] = 'Coquille Vide',
		['coince'] = 'Coincé',
		['perdu'] = 'Perdu',
		['intimidant'] = 'Intimidant',
		['richissime'] = 'Richissime',
		['hargneux'] = 'Hargneux',
		['imposant'] = 'Imposant',
		['frimeur'] = 'Frimeur',
		
	-- Others
	
		['randonneur'] = 'Randonneur',		
		['blesse'] = 'Blessé',
		['obese'] = 'Obèse',			
		['fesses'] = 'Mal aux fesses',
		['detente'] = 'Detente',
		
	-- Special Women
	
		['arrogante'] = 'Arrogante',
		['classique'] = 'Classique',
		['fragile'] = 'Fragile',
		['enerve'] = 'Enervée',
		['fatale'] = 'Femme fatale',
		['fuite'] = 'Fuite',
		['tristesse'] = 'Tristesse',
		['rebelle'] = 'Rebelle',
		['serieuse'] = 'Serieuse',
		['ffesses'] = 'Roule des fesses',
		['hautaines'] = 'Hautaine',
}
