
local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData							=	{}
local HasAlreadyEnteredMarker	=	false
local first 									=	false
local isInStock 							= false
local isInActions 						= false
local isInTeleport 						= false
local isInRadio 							= false
local isInPos 								= false
local isInMarker				 			= false
local radio 									= true
local spawnVehicleByName		  = false
local bool  									= false
ESX														=	nil

Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)

-- TELEPORTERS
AddEventHandler('magical_teleport:teleportMarkers', function(position)
	if IsPedSittingInAnyVehicle(GetPlayerPed(-1)) then
		entity = GetVehiclePedIsIn(GetPlayerPed(-1), false)
		SetEntityCoords(entity, position.x, position.y, position.z)
		TaskWarpPedIntoVehicle(GetPlayerPed(-1),  entity,  -1)
	else
		SetEntityCoords(GetPlayerPed(-1), position.x, position.y, position.z)
	end
end)

-- Show top left hint
Citizen.CreateThread(function()
	while true do
    Wait(0)
		if hintIsShowed == true then
		  SetTextComponentFormat("STRING")
		  AddTextComponentString(hintToDisplay)
		  DisplayHelpTextFromStringLabel(0, 0, 1, -1)
		end
	end
end)

-- Display teleport markers
Citizen.CreateThread(function()
	while true do
    Wait(0)
	local coords = GetEntityCoords(GetPlayerPed(-1))
		for k,v in pairs(Config.TeleportZones) do
			if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) or (GetDistanceBetweenCoords(coords, v.Stock.x, v.Stock.y, v.Stock.z, true) < Config.DrawDistance) or  (GetDistanceBetweenCoords(coords, v.Teleport.x, v.Teleport.y, v.Teleport.z, true) < Config.DrawDistance) or (GetDistanceBetweenCoords(coords, v.Actions.x, v.Actions.y, v.Actions.z, true) < Config.DrawDistance) then
				if gang == v.Job or gang == 'chef' .. v.Job then
					if (GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
						DrawMarker(v.Marker, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
					end
					if (GetDistanceBetweenCoords(coords, v.Stock.x, v.Stock.y, v.Stock.z, true) < Config.DrawDistance) then
						DrawMarker(v.Marker, v.Stock.x, v.Stock.y, v.Stock.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
					end
					if  (GetDistanceBetweenCoords(coords, v.Teleport.x, v.Teleport.y, v.Teleport.z, true) < Config.DrawDistance) then
						DrawMarker(v.Marker, v.Teleport.x, v.Teleport.y, v.Teleport.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
					end
					if gang == 'chef' .. v.Job and (GetDistanceBetweenCoords(coords, v.Actions.x, v.Actions.y, v.Actions.z, true) < Config.DrawDistance) then
						DrawMarker(v.Marker, v.Actions.x, v.Actions.y, v.Actions.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
					end
				end
			end
		end
	end
end)

-- Activate teleport marker
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)

		local coords      = GetEntityCoords(GetPlayerPed(-1))
		local position    = nil
		local zone        = nil

		if not first then
			ESX.TriggerServerCallback('magical_teleport:test', function(data)
				gang = data
				first = true
			end)

			ESX.TriggerServerCallback('magical_teleport:recuperationsomme', function(data)
				resultat = data
			end)
		end

		for k,v in pairs(Config.TeleportZones) do
			if(GetDistanceBetweenCoords(coords, v.Radio.x, v.Radio.y, v.Radio.z, true) < v.Size.x) or (GetDistanceBetweenCoords(coords, v.Stock.x, v.Stock.y, v.Stock.z, true) < v.Size.x) or (GetDistanceBetweenCoords(coords, v.Teleport.x, v.Teleport.y, v.Teleport.z, true) < v.Size.x) or (GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) or (GetDistanceBetweenCoords(coords, v.Actions.x, v.Actions.y, v.Actions.z, true) < v.Size.x) then
				if gang == v.Job or gang == 'chef' .. v.Job then
					if (GetDistanceBetweenCoords(coords, v.Stock.x, v.Stock.y, v.Stock.z, true) < v.Size.x) then
						hint = _U('open_vault')
						isInStock = true
					elseif (GetDistanceBetweenCoords(coords, v.Teleport.x, v.Teleport.y, v.Teleport.z, true) < v.Size.x) then
						isInTeleport = true
						position = v.Pos
						hint = _U('e_to_exit_1')
					elseif (GetDistanceBetweenCoords(coords, v.Radio.x, v.Radio.y, v.Radio.z, true) < v.Size.x) then
						hint = _U('e_to_radio')
						emetteur = v.Radio.emetteur
						isInRadio = true
					elseif (GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
						isInPos = true
						position = v.Teleport
						zone = v
						hint = _U('e_to_enter_1')
					elseif gang == 'chef' .. v.Job and (GetDistanceBetweenCoords(coords, v.Actions.x, v.Actions.y, v.Actions.z, true) < v.Size.x) then
						isInActions = true
						hint = _U('open_action')
						coordsspawning = v.Teleport
					end
					isInMarker = true
					break
				end
			else
				isInMarker  = false
				isInRadio  = false
				isInStock  = false
				isInTeleport  = false
				isInPos  = false
				isInActions  = false
			end
		end

		if IsControlJustReleased(0, Keys["E"]) and (isInPos or isInTeleport) then
			TriggerEvent('magical_teleport:teleportMarkers', position)
		end

		if IsControlJustReleased(0, Keys["E"]) then
			if isInStock then
				OpenStock()
			elseif isInActions then
				OpenStock2()
			elseif isInRadio then
				OpenRadio()
			end
		end

		if isInMarker then
			hintToDisplay = hint
			hintIsShowed = true
		else
			if not isInMarker then
				hintToDisplay = "no hint to display"
				hintIsShowed = false
			end
		end
	end
end)

function OpenRadio()

	if emetteur == "biker1" then
		emetteur1 = "SE_bkr_biker_dlc_int_01_BAR"
		emetteur2 = "SE_bkr_biker_dlc_int_01_GRG"
		emetteur3 = "SE_bkr_biker_dlc_int_01_REC"
	end
	if emetteur == "biker2" then
		emetteur1 = "SE_bkr_biker_dlc_int_02_GRG"
		emetteur2 = "SE_bkr_biker_dlc_int_02_REC"
		emetteur3 = ""
	end

	local elements = {}

	if not radio then
		table.insert(elements, {label = '<b>Allumer la radio</b>', value = 'on'})
	else
		table.insert(elements, {label = '<b>Eteindre la radio</b>', value = 'off'})
	end

	table.insert(elements, {label = '------'})
	table.insert(elements, {label = 'Radio Country', 			value = 'country'})
	table.insert(elements, {label = 'Radio Reggae',  			value = 'reggae'})
	table.insert(elements, {label = 'Radio Classic Rock', value = 'crock'})
	table.insert(elements, {label = 'Radio Modern Rock', 	value = 'mrock'})
	table.insert(elements, {label = 'Radio Hip Hop', 			value = 'hiphop'})
	table.insert(elements, {label = 'Radio Bar', 		 			value = 'bar'})

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'mecano_actions',
		{
			title    = 'Radio',
			elements = elements
		},
		function(data, menu)
			if data.current.value == 'on' then
				menu.close()
				radio = true
				SetStaticEmitterEnabled(emetteur1, true)
				SetStaticEmitterEnabled(emetteur2, true)
				SetStaticEmitterEnabled(emetteur3, true)
			end

			if data.current.value == 'off' then
				menu.close()
				radio = false
				SetStaticEmitterEnabled(emetteur1, false)
				SetStaticEmitterEnabled(emetteur2, false)
				SetStaticEmitterEnabled(emetteur3, false)
			end

			if data.current.value == 'country' then
				menu.close()
				SetEmitterRadioStation(emetteur1, "HIDDEN_RADIO_06_COUNTRY")
				SetEmitterRadioStation(emetteur2, "HIDDEN_RADIO_06_COUNTRY")
				SetEmitterRadioStation(emetteur3, "HIDDEN_RADIO_06_COUNTRY")
			end

			if data.current.value == 'reggae' then
				menu.close()
				SetEmitterRadioStation(emetteur1, "HIDDEN_RADIO_12_REGGAE")
				SetEmitterRadioStation(emetteur2, "HIDDEN_RADIO_12_REGGAE")
				SetEmitterRadioStation(emetteur3, "HIDDEN_RADIO_12_REGGAE")
			end

			if data.current.value == 'crock' then
				menu.close()
				SetEmitterRadioStation(emetteur1, "HIDDEN_RADIO_BIKER_CLASSIC_ROCK")
				SetEmitterRadioStation(emetteur2, "HIDDEN_RADIO_BIKER_CLASSIC_ROCK")
				SetEmitterRadioStation(emetteur3, "HIDDEN_RADIO_BIKER_CLASSIC_ROCK")
			end

			if data.current.value == 'mrock' then
				menu.close()
				SetEmitterRadioStation(emetteur1, "HIDDEN_RADIO_BIKER_MODERN_ROCK")
				SetEmitterRadioStation(emetteur2, "HIDDEN_RADIO_BIKER_MODERN_ROCK")
				SetEmitterRadioStation(emetteur3, "HIDDEN_RADIO_BIKER_MODERN_ROCK")
			end

			if data.current.value == 'hiphop' then
				menu.close()
				SetEmitterRadioStation(emetteur1, "HIDDEN_RADIO_BIKER_HIP_HOP")
				SetEmitterRadioStation(emetteur2, "HIDDEN_RADIO_BIKER_HIP_HOP")
				SetEmitterRadioStation(emetteur3, "HIDDEN_RADIO_BIKER_HIP_HOP")
			end

			if data.current.value == 'bar' then
				menu.close()
				SetEmitterRadioStation(emetteur1, "HIDDEN_RADIO_STRIP_CLUB")
				SetEmitterRadioStation(emetteur2, "HIDDEN_RADIO_STRIP_CLUB")
				SetEmitterRadioStation(emetteur3, "HIDDEN_RADIO_STRIP_CLUB")
			end
		end,
		function(data, menu)
			menu.close()
		end
	)
end


function OpenStock2()
	local elements = {
		{label = 'Vehicule test', value = 'vehicle_list'},
		{label = 'Message de gang', value = 'message'},
		{label = 'Liste des membres', value = 'gang_list'},
		{label = 'Recrutement', value = 'gang_recrute'},
	}

	ESX.UI.Menu.CloseAll()
	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'mecano_actions',
		{
			title    = 'Gestion de Gang',
			elements = elements
		},
		function(data, menu)

			if data.current.value == 'vehicle_list' then
				local elements = {
					{label = 'Rat Bike', value = 'ratbike'},
					{label = 'BF400', value = 'bf400'}
				}

				ESX.UI.Menu.CloseAll()
				ESX.UI.Menu.Open(
					'default', GetCurrentResourceName(), 'spawn_vehicle',
					{
						title    = 'Véhicule de service',
						elements = elements
					},
					function(data, menu)
						for i=1, #elements, 1 do

							local playerPed = GetPlayerPed(-1)

							ESX.Game.SpawnVehicle(data.current.value, coordsspawning, 90.0, function(vehicle)
								SetVehicleNumberPlateText(vehicle, " ")
								TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
							end)
							break
						end
						menu.close()
					end,
					function(data, menu)
						menu.close()
						OpenStock2()
					end
				)
			end

			if data.current.value == 'message' then
				menu.close()
				spawnVehicleByName = true
			end

			if data.current.value == 'gang_list' then
				menu.close()
				OpenEmployeeList()
			end

			if data.current.value == 'gang_recrute' then
				menu.close()
				local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

				if closestDistance == -1 or closestDistance > 3.0 then
					ESX.ShowNotification('Aucune personne n\'est près de vous !')
				else
					local recrue = GetPlayerServerId(closestPlayer)
					TriggerServerEvent('magical_teleport:setjob', recrue, gang)
					ESX.ShowNotification('Vous avez ~b~embauché~s~ ')
				end
			end
		end,
		function(data, menu)
			menu.close()
		end
	)
end

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)

		if spawnVehicleByName then
			DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8", "", "", "", "", "", 60)
			blockinput = true

			while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
				Citizen.Wait(0)
			end

			if UpdateOnscreenKeyboard() ~= 2 then
				local result = GetOnscreenKeyboardResult()
				  ESX.TriggerServerCallback('esx_society:getEmployees', function(employees)
					TriggerEvent('esx:showNotification',  result)
					TriggerServerEvent('eden_gofast:itemremove2', result,employees)
				end)
			end

			blockinput = false
			spawnVehicleByName = false
		end
	end
end)

function OpenEmployeeList()

  ESX.TriggerServerCallback('esx_society:getEmployees', function(employees)
    local elements = {
      head = {('Membre'), ('Actions')},
      rows = {}
    }

    for i=1, #employees, 1 do
      table.insert(elements.rows, {
        data = employees[i],
        cols = {
          employees[i].name,
          '{{' .. ('Passation de pouvoir') .. '|promote}} {{' .. ('Virer du gang') .. '|fire}}'
        }
      })
    end

    ESX.UI.Menu.Open(
      'list', GetCurrentResourceName(), 'employee_list_' ,
      elements,
      function(data, menu)

        local employee = data.data
        if data.value == 'promote' then
					menu.close()
        end

        if data.value == 'fire' then
					menu.close()
					TriggerServerEvent('eden_animal:puteuh', employee.identifier)
					TriggerEvent('esx:showNotification', ('Vous avez virer ' .. employee.identifier))
        end
      end,
      function(data, menu)
        menu.close()
        OpenManageEmployeesMenu()
      end
    )
  end)
end

function OpenStock()
	ESX.TriggerServerCallback('magical_teleport:recuperationsomme', function(data)
		Citizen.Trace(data)

		local elements = {
			--{label = 'Sortir un véhicule', value = 'vehicle_list'},
			{label = '<span style="color:blue;"><B>--- Argent Propre ---</B></span>'},
			{label = '<span style="color:grey;">< Montant du coffre : $' .. data .. ' ></span>'},
			{label = 'Retirer argent gang', value = 'withdraw_society_money'},
			{label = 'Déposer argent ',        value = 'deposit_money'},
			{label = '<span style="color:blue;"><B>--- Coffre ---</B></span>'},
			{label = 'Retirer du coffre', value = 'society_inventory'},
			{label = 'Deposer dans le coffre', value = 'player_inventory'},
			{label = '<span style="color:blue;"><B>--- Vestiaires ---</B></span>'},
			{label = 'Tenue de gang', value = 'cloakroom'},
			{label = 'Tenue civile', value = 'cloakroom2'},
		}

		ESX.UI.Menu.CloseAll()

		ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'mecano_actions',
			{
				title    = 'Coffre de Gang',
				elements = elements
			},
			function(data, menu)

				if data.current.value == 'cloakroom' then
					menu.close()
					ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
					local playerPed = GetPlayerPed(-1)

    			if skin.sex == 0 then
						SetPedComponentVariation(GetPlayerPed(-1), 8, 23, 1, 0)--Teeshrit
						SetPedComponentVariation(GetPlayerPed(-1), 11, 166, 0, 0) --Veste
						SetPedComponentVariation(GetPlayerPed(-1), 3, 12, 0, 0)--Bras
						SetPedComponentVariation(GetPlayerPed(-1), 4, 72,0, 0)--Pantalon
						SetPedComponentVariation(GetPlayerPed(-1), 6, 25, 0, 0)--Chaussures
					else
						SetPedComponentVariation(GetPlayerPed(-1), 8, 23, 1, 0)--Teeshrit
						SetPedComponentVariation(GetPlayerPed(-1), 11, 166, 0, 0) --Veste
						SetPedComponentVariation(GetPlayerPed(-1), 3, 12, 0, 0)--Bras
						SetPedComponentVariation(GetPlayerPed(-1), 4, 72,0, 0)--Pantalon
						SetPedComponentVariation(GetPlayerPed(-1), 6, 25, 0, 0)--Chaussures
    			end
				end)
			end

			if data.current.value == 'cloakroom2' then
				menu.close()
				ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)

    			TriggerEvent('skinchanger:loadSkin', skin)
				end)
			end

			if data.current.value == 'withdraw_society_money' then
				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'withdraw_society_money_amount',
					{
						title = 'Montant du retrait'
					},
					function(data, menu)
						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant invalide')
						else
							menu.close()
							TriggerServerEvent('esx_society:withdrawMoney', 'gang1', amount)
						end
					end,
					function(data, menu)
						menu.close()
					end
				)
				menu.close()
			end

			if data.current.value == 'deposit_money' then
				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'deposit_money_amount',
					{
						title = 'Montant du dépôt'
					},
					function(data, menu)
						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant invalide')
						else
							menu.close()
							TriggerServerEvent('esx_society:depositMoney', 'gang1', amount)
						end
					end,
					function(data, menu)
						menu.close()
					end
				)
				menu.close()
			end

			if data.current.value == 'society_inventory' then
				OpenRoomInventoryMenu()
			end

			if data.current.value == 'player_inventory' then
				OpenPlayerInventoryMenu()
			end
		end,
		function(data, menu)
			menu.close()
		end
	end)
end

function OpenRoomInventoryMenu()

  local societyName = 'society_gang1'
  local societyJobName = 'gang1'
  local societyBlackMoneyName = 'society_gang1_black_money'

  local blackMoney = 0
  local items = {}
  local weapons = {}
  local elements = {}

  ESX.TriggerServerCallback('esx_society:getAccountMoney', function(money)

    blackMoney = money
		table.insert(elements, {label = _U('dirty_money') .. blackMoney .. '$', type = 'item_account', value = 'black_money'})
  end, societyBlackMoneyName)

  ESX.TriggerServerCallback('esx_society:getAccountItems', function(items)

    for i=1, #items, 1 do
	  	if items[i].count > 0 then
        table.insert(elements, {label = items[i].label .. ' x' .. items[i].count, type = 'item_standard', value = items[i].name})
    	end
		end
	end, societyJobName)

	ESX.TriggerServerCallback('esx_jobs:getSocietyWeapons', function(weapons)

		for i=1, #weapons, 1 do
			if weapons[i].count > 0 then
				table.insert(elements, {label = ESX.GetWeaponLabel(weapons[i].name) .. ' x' .. weapons[i].count, type = 'item_weapon', value = weapons[i].name})
			end
		end
	end, societyName)

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'stocksItems_menu',
		{
		title    = _U('societyInventory'),
		align    = 'top-left',
		elements = elements
		},
		function(data, menu)

			if data.current.type == 'item_weapon' then

				menu.close()

				ESX.TriggerServerCallback('esx_jobs:getWeapon', function()
					OpenRoomInventoryMenu()
				end, data.current.value, societyJobName)

				ESX.SetTimeout(300, function()
					OpenRoomInventoryMenu()
				end)
			else

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
					{
						title = _U('amount')
					},
					function(data2, menu)

						local count = tonumber(data2.value)
						menu.close()

						if count == nil then
							ESX.ShowNotification(_U('amount_invalid'))
						else
							TriggerServerEvent('esx_jobs:getItem', data.current.type, data.current.value, count, societyJobName)
							OpenRoomInventoryMenu()
						end
					end,
					function(data2, menu2)
						menu2.close()
					end
				)
			end
		end,
		function(data, menu)
			menu.close()
		end
  )
end

function OpenPlayerInventoryMenu()

  local societyJobName = 'gang1'

  ESX.TriggerServerCallback('esx_jobs:getPlayerInventory', function(inventory)

    local elements = {}
    table.insert(elements, {label = ('Argent sale') .. inventory.blackMoney .. '$', type = 'item_account', value = 'black_money'})

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end
    end

    local playerPed  = GetPlayerPed(-1)
    local weaponList = ESX.GetWeaponList()

    for i=1, #weaponList, 1 do

      local weaponHash = GetHashKey(weaponList[i].name)

      if HasPedGotWeapon(playerPed,  weaponHash,  false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
        local ammo = GetAmmoInPedWeapon(playerPed, weaponHash)
        table.insert(elements, {label = weaponList[i].label, type = 'item_weapon', value = weaponList[i].name, ammo = ammo})
      end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'player_inventory',
      {
        title    = _U('playerInventory'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        if data.current.type == 'item_weapon' then
          menu.close()

          ESX.TriggerServerCallback('esx_jobs:putWeapon', function()
			  		OpenPlayerInventoryMenu()
		  		end, data.current.value, societyJobName)

          ESX.SetTimeout(300, function()
            OpenPlayerInventoryMenu()
          end)
        else

          ESX.UI.Menu.Open(
            'dialog', GetCurrentResourceName(), 'put_item_count',
            {
              title = _U('amount'),
            },
            function(data2, menu)

              menu.close()
              TriggerServerEvent('esx_jobs:putItem', data.current.type, data.current.value, tonumber(data2.value), societyJobName)

              ESX.SetTimeout(300, function()
                OpenPlayerInventoryMenu()
              end)
            end,
            function(data2,menu)
              menu.close()
            end
          )
        end
      end,
      function(data, menu)
        menu.close()
      end
    )
  end)
end

AddEventHandler('playerSpawned', function()
interiorID = GetInteriorAtCoords(483.2, 4810.5, -58.9)

  if IsValidInterior(interiorID) then
  	EnableInteriorProp(interiorID, "set_int_02_decal_01")
  	EnableInteriorProp(interiorID, "set_int_02_lounge1")
  	EnableInteriorProp(interiorID, "set_int_02_cannon")
  	EnableInteriorProp(interiorID, "set_int_02_clutter1")
  	EnableInteriorProp(interiorID, "set_int_02_crewemblem")
  	EnableInteriorProp(interiorID, "set_int_02_shell")
  	EnableInteriorProp(interiorID, "set_int_02_security")
  	EnableInteriorProp(interiorID, "set_int_02_sleep")
  	EnableInteriorProp(interiorID, "set_int_02_trophy1")
  	EnableInteriorProp(interiorID, "set_int_02_paramedic_complete")
  	EnableInteriorProp(interiorID, "set_Int_02_outfit_paramedic")
  	EnableInteriorProp(interiorID, "set_Int_02_outfit_serverfarm")
  	SetInteriorPropColor(interiorID, "set_int_02_decal_01", 1)
  	SetInteriorPropColor(interiorID, "set_int_02_lounge1", 1)
  	SetInteriorPropColor(interiorID, "set_int_02_cannon", 1)
  	SetInteriorPropColor(interiorID, "set_int_02_clutter1", 1)
  	SetInteriorPropColor(interiorID, "set_int_02_shell", 1)
  	SetInteriorPropColor(interiorID, "set_int_02_security", 1)
  	SetInteriorPropColor(interiorID, "set_int_02_sleep", 1)
  	SetInteriorPropColor(interiorID, "set_int_02_trophy1", 1)
  	SetInteriorPropColor(interiorID, "set_int_02_paramedic_complete", 1)
  	SetInteriorPropColor(interiorID, "set_Int_02_outfit_paramedic", 1)
  	SetInteriorPropColor(interiorID, "set_Int_02_outfit_serverfarm", 1)
    RefreshInterior(interiorID)
  end

  interiorID = GetInteriorAtCoords(-1266.0, -3014.0, -47.0)

  if IsValidInterior(interiorID) then
  	EnableInteriorProp(interiorID, "set_lighting_hangar_a")
  	EnableInteriorProp(interiorID, "set_tint_shell")
  	EnableInteriorProp(interiorID, "set_bedroom_tint")
  	EnableInteriorProp(interiorID, "set_crane_tint")
  	EnableInteriorProp(interiorID, "set_modarea")
  	EnableInteriorProp(interiorID, "set_lighting_tint_props")
  	EnableInteriorProp(interiorID, "set_floor_1")
  	EnableInteriorProp(interiorID, "set_floor_decal_1")
  	EnableInteriorProp(interiorID, "set_bedroom_modern")
  	EnableInteriorProp(interiorID, "set_office_modern")
  	EnableInteriorProp(interiorID, "set_bedroom_blinds_open")
  	EnableInteriorProp(interiorID, "set_lighting_wall_tint01")
  	SetInteriorPropColor(interiorID, "set_tint_shell", 1)
  	SetInteriorPropColor(interiorID, "set_bedroom_tint", 1)
  	SetInteriorPropColor(interiorID, "set_crane_tint", 1)
  	SetInteriorPropColor(interiorID, "set_modarea", 1)
  	SetInteriorPropColor(interiorID, "set_lighting_tint_props", 1)
  	SetInteriorPropColor(interiorID, "set_floor_decal_1", 1)
  	RefreshInterior(interiorID)
  end
end)

AddEventHandler('playerSpawned', function()
	if bool == false then
		-- Biker1
		EnableInteriorProp(246273, "meth_stash3")
		EnableInteriorProp(246273, "walls_02")
		EnableInteriorProp(246273, "mural_01")
		EnableInteriorProp(246273, "gun_locker")
		EnableInteriorProp(246273, "furnishings_01")
		EnableInteriorProp(246273, "cash_stash3")
		EnableInteriorProp(246273, "decorative_02")
		RefreshInterior(246273)
		-- Vehicle Warehouse
		EnableInteriorProp(252673, "basic_style_set")
		EnableInteriorProp(252673, "car_floor_hatch")
		RefreshInterior(252673)
		--Biker2
		EnableInteriorProp(246529, "walls_02")
		EnableInteriorProp(246529, "lower_walls_default")
		EnableInteriorProp(246529, "mural_01")
		EnableInteriorProp(246529, "furnishings_01")
		EnableInteriorProp(246529, "cash_medium")
		EnableInteriorProp(246529, "coke_medium")
		EnableInteriorProp(246529, "counterfeit_medium")
		RefreshInterior(246529)
		--Bunker
		EnableInteriorProp(258561,"standard_bunker_set")
		EnableInteriorProp(258561,"standard_bunker_set")
		EnableInteriorProp(258561,"Bunker_Style_C")
		EnableInteriorProp(258561,"Office_Upgrade_set")
		EnableInteriorProp(258561,"Gun_schematic_set")
		EnableInteriorProp(258561,"security_upgrade")
		EnableInteriorProp(258561,"gun_range_lights")
		EnableInteriorProp(258561,"gun_locker_upgrade")
		RefreshInterior(258561)
	end
end)
