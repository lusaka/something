ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

ESX.RegisterServerCallback("magical_teleport:test", function(_source , cb)
	local source        = _source
	local xPlayer        = ESX.GetPlayerFromId(source)
    MySQL.Async.fetchAll(
      'SELECT * FROM users WHERE identifier = @identifier',
      {
        ['@identifier'] = xPlayer.identifier
      },
      function(result)
        cb(result[1].gang)
      end
    )
end)

ESX.RegisterServerCallback("magical_teleport:recuperationsomme", function(_source , cb)
	local source      = _source
	local xPlayer     = ESX.GetPlayerFromId(source)
	MySQL.Async.fetchAll(
    'SELECT money FROM addon_account_data WHERE account_name = @identifier',
    {
      ['@identifier'] = "society_gang1",
    },
    function(result)
      cb(result[1].money)
    end
  )
end)

ESX.RegisterServerCallback('esx_society:getAccountMoney', function(source, cb, account)
	TriggerEvent('esx_addonaccount:getSharedAccount', account, function(account)
		cb(account.money)
	end)
end)

ESX.RegisterServerCallback('esx_society:getAccountItems', function(source, cb, account)
	TriggerEvent('esx_addoninventory:getSharedInventory', account, function(inventory)
		cb(inventory.items)
	end)
end)

RegisterServerEvent('esx_jobs:getItem')
AddEventHandler('esx_jobs:getItem', function(type, itemName, count, society)

  local _source      = source
  local xPlayer      = ESX.GetPlayerFromId(_source)
  local society		 = society

  if type == 'item_standard' then

    TriggerEvent('esx_addoninventory:getSharedInventory', 'society_' .. society, function(inventory)

  		local item = inventory.getItem(itemName)

  		if item.count >= count then
  			inventory.removeItem(itemName, count)
  			xPlayer.addInventoryItem(itemName, count)

  			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_removed') .. count .. ' ' .. item.label)
  		else
  			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('amount_invalid'))
  		end
    end)
  end

  if type == 'item_account' then

    local xPlayer = ESX.GetPlayerFromId(source)

  	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_' .. society .. '_black_money', function(account)

  		if count > 0 and account.money >= count then

  			account.removeMoney(count)
  			xPlayer.addAccountMoney(itemName, count)

  			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_removed') .. count .. _U('of_dirty_money'))
  		else
  			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('amount_invalid'))
  		end
  	end)
  end
end)

ESX.RegisterServerCallback('esx_jobs:getPlayerInventory', function(source, cb)

  local xPlayer    = ESX.GetPlayerFromId(source)
  local blackMoney = xPlayer.getAccount('black_money').money
  local items      = xPlayer.inventory

  cb({
    blackMoney = blackMoney,
    items      = items
  })
end)

RegisterServerEvent('esx_jobs:putItem')
AddEventHandler('esx_jobs:putItem', function(type, item, count, society)

  local _source      = source
  local xPlayer      = ESX.GetPlayerFromId(_source)

  if type == 'item_standard' then

  	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_' .. society, function(inventory)

  		local items = inventory.getItem(item)
  		local playerItemCount = xPlayer.getInventoryItem(item).count

  		if playerItemCount >= count then
  			xPlayer.removeInventoryItem(item, count)
  			inventory.addItem(item, count)

  			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_added') .. count .. ' ' .. items.label)
  		else
  			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('amount_invalid'))
  		end
  	end)
  end

  if type == 'item_account' then

  	if count > 0 and xPlayer.getAccount('black_money').money >= count then

  		TriggerEvent('esx_addonaccount:getSharedAccount', 'society_' .. society .. '_black_money', function(account)
  			xPlayer.removeAccountMoney(item, count)
  			account.addMoney(count)
  		end)

  		TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_added') .. count .. _U('of_dirty_money'))
  	else
  		TriggerClientEvent('esx:showNotification', xPlayer.source, _U('amount_invalid'))
  	end
  end
end)

ESX.RegisterServerCallback('esx_jobs:putWeapon', function(source, cb, weaponName, society)

	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeWeapon(weaponName)

	TriggerEvent('esx_datastore:getSharedDataStore', 'society_'  .. society, function(store)

		local weapons = store.get('weapons')

		if weapons == nil then
			weapons = {}
		end

		local foundWeapon = false

		for i=1, #weapons, 1 do
			if weapons[i].name == weaponName then
				weapons[i].count = weapons[i].count + 1
				foundWeapon = true
			end
		end

		if not foundWeapon then
			table.insert(weapons, {
				name  = weaponName,
				count = 1
			})
		end

		store.set('weapons', weapons)
		cb()
	end)
end)

ESX.RegisterServerCallback('esx_jobs:getSocietyWeapons', function(source, cb, society)

	TriggerEvent('esx_datastore:getSharedDataStore', 'society_' .. society, function(store)

		local weapons = store.get('weapons')

		if weapons == nil then
			weapons = {}
		end

		cb(weapons)
	end)
end)

ESX.RegisterServerCallback('esx_jobs:getWeapon', function(source, cb, weaponName, society)

	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.addWeapon(weaponName, 1000)

	TriggerEvent('esx_datastore:getSharedDataStore', 'society_' .. society, function(store)

		local weapons = store.get('weapons')

		if weapons == nil then
			weapons = {}
		end

		local foundWeapon = false

		for i=1, #weapons, 1 do
			if weapons[i].name == weaponName then
				weapons[i].count = (weapons[i].count > 0 and weapons[i].count - 1 or 0)
				foundWeapon = true
			end
		end

		if not foundWeapon then
			table.insert(weapons, {
				name  = weaponName,
				count = 0
			})
		end

		store.set('weapons', weapons)
		cb()
	end)
end)

ESX.RegisterServerCallback('esx_society:getEmployees', function(source, cb, society)

  MySQL.Async.fetchAll(
    'SELECT * FROM users WHERE gang = @job ',
    { ['@job'] = "mecano" },
    function (result)
      local employees = {}
       for i=1, #result, 1 do
        table.insert(employees, {
          name        = result[i].name,
          identifier  = result[i].identifier,
        })
      end
      cb(employees)
    end
  )
end)

RegisterServerEvent("magical_teleport:puteuh")
AddEventHandler("magical_teleport:puteuh", function(identifier)

	MySQL.Async.execute(
		'UPDATE users SET gang = "(NULL)" WHERE identifier = @identifier',
		{
			['@identifier']    = identifier
		}
	)
end)

RegisterServerEvent('magical_teleport:setjob')
AddEventHandler('magical_teleport:setjob', function(id, gang)
	local xPlayer = ESX.GetPlayerFromId(id)

		MySQL.Async.execute(
		'UPDATE users SET gang = @gang WHERE identifier = @identifier',
		{
			['@identifier']    = xPlayer.identifier,
			['@gang']    = gang
		}
	)
end)

RegisterServerEvent('eden_gofast:itemremove2')
AddEventHandler('eden_gofast:itemremove2', function(message,employees)

	local _source		= source
	local xPlayer  		= ESX.GetPlayerFromId(source)
	local xPlayers 		= ESX.GetPlayers()
	local pack_drugs 	= xPlayer.getInventoryItem('pack_drugs').count
	local time 		     = os.date("%d/%m/%y %X")


  for i=1, #xPlayers, 1 do
    for n=1, #employees, 1 do
      local xPlayer2 = ESX.GetPlayerFromId(xPlayers[i])

      if xPlayer2.identifier == employees[n].identifier then
        TriggerClientEvent('esx:showNotification', xPlayers[i], ('<B>' .. message ..  '</B>'))
      end
    end
  end

	print('Message de gang par : ' ..xPlayer.name .. ' a : ' .. time .. '// Message : '..message..'$' )
end)
