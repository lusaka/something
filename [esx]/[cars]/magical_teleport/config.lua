Config                            = {}
Config.DrawDistance               = 100.0

Config.Locale                     = 'fr'


-----------------
----- TELEPORTERS -----

Config.TeleportZones = {

	--- GARAGE ---
	
	EntreBase1 = {
	Pos =  {x=-1076.8215332031,  y=-2885.8581542969, z=12.964534759521},
	Stock = {x = 963.53228759766,y = -2997.4072265625,z = -40.646949768066 },
	Actions ={x = 964.45031738281,y = -3003.3928222656,z = -40.639892578125 },
	Radio = {},
    Size      = { x = 2.0, y = 2.0, z = 0.2 },
    Color     = { r = 204, g = 204, b = 0 },
    Marker    = 1,
    Hint      = _U('e_to_enter_1'),
    Teleport  = { x = 994.5925, y = -3002.594, z = -39.64699 },
	Job = "mecano",
  },
  
	--- CLUBHOUSE 2 --


    EnterBiker1 = {
    Pos			= {x=-1053.915, y=-2888.616,  z=12.953},
	Stock		= {x = 1004.3748779297,y = -3149.7507324219,z = -39.907119750977 },
	Actions 	= {x = 1007.8698730469,y = -3169.875,z = -39.90710067749 },
	Radio = {x = 1000.9478149414,y = -3171.2543945313,z = -35.077438354492, emetteur = "biker2" },
    Size      	= {x = 2.0,y = 2.0,z = 0.2 },
    Color     = { r = 204, g = 204, b = 0 },
    Marker    = 1,
    Hint      = _U('e_to_enter_1'),
    Teleport  = {x = 998.12066650391,y = -3164.587890625,z = -39.907325744629 },

	Job = "mecano",
  },
  
  
  -- THE BUNKER --
  
  
   EnterBase2 = {
	Pos =  {x=-1082.611,  y=-2882.405, z=12.986},
	Stock = {x = 908.69311523438,y = -3210.7626953125,z = -99.222114562988 },
	Radio = {},
	Actions 	  =	{x = 904.07257080078,y = -3201.5014648438,z = -98.187957763672 },
    Size      = { x = 2.0, y = 2.0, z = 0.2 },
    Color     = { r = 204, g = 204, b = 0 },
    Marker    = 1,
    Hint      = _U('e_to_enter_1'),
    Teleport  = { x = 894.797, y = -3244.699, z = -96.258 },
	Job = "mecano"
  },

  --- CLUBHOUSE 1 --
  EnterBiker2 = {
    Pos       = {x=-1067.800,  y=-2884.989, z=12.964},
	Stock 	  =	{x = 1119.0993652344,y = -3143.5900878906,z = -38.062740325928 },
	Actions 	  =	{x = 1116.2840576172,y = -3161.2377929688,z = -37.870491027832 },
	Radio = {x=1122.59, y=-3153.40, z=-37.06, emetteur = "biker1"},
    Size      = { x = 2.0, y = 2.0, z = 0.2 },
    Color     = { r = 204, g = 204, b = 0 },
    Marker    = 1,
    Hint      = _U('e_to_enter_1'),
    Teleport  = {x = 1110.4898681641,y = -3166.0319824219,z = -38.518589019775 },
	Job = "mecano"
  },

  
}
