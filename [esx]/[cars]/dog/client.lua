ESX = nil
local dog = nil
local following = false
local inVehicle = false
local boolbreak = false
local search = true
local lastTarget = nil
local PlayerData = {}
local food = 100
local eat = false

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)
--Event
RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	PlayerData = xPlayer
end)

AddEventHandler("spawndogydogey", function() --- Spawn du chien
	local dogmodel = nil
	Citizen.CreateThread(function()
		ESX.UI.Menu.CloseAll()
		local elements = {}
		table.insert(elements, {label = 'chien1',             					value = -1788665315})
		table.insert(elements, {label = 'chien2(pas défini)',							value = nil})
		table.insert(elements, {label = 'chien3(pas défini)..',			value = nil})
		table.insert(elements, {label = 'Rentrer le chien',			value = "deleter"})
		ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'dogspawnmenu',
			{
				title = 'Choix du chien',
				align = 'top-left',
				elements = elements
			},
		    function(data, menu) --Submit Cb
		    	if data.current.value == "deleter" then
		    		TriggerEvent("deletedogydogey")
		    	else
                	dogmodel = data.current.value
                end
        	end,
       		function(data, menu) --Cancel Cb
                menu.close()
        	end,
       		function(data, menu) --Change Cb
                print(data.current.value)
       		end
		)

		if dog == nil then
			eat = false
			following = false
			inVehicle = false
			food = 100
			RequestModel(dogmodel)
			while not HasModelLoaded(dogmodel) do
				RequestModel(dogmodel)
				Citizen.Wait(100)
			end

			local plypos = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, -2.0, 0.0)
			local plyhead = GetEntityHeading(GetPlayerPed(PlayerId()))
			local spawned = CreatePed(28, dogmodel, plypos.x, plypos.y, plypos.z, plyhead, 1, 1)					
			GiveWeaponToPed(spawned, GetHashKey("WEAPON_ANIMAL"), 200, true, true)
			SetBlockingOfNonTemporaryEvents(spawned,true)
			SetPedFleeAttributes(spawned, 0, false)
			dog = spawned
			
			SetEntityAsMissionEntity(dog, true,true)
		end
	end)
end)
RegisterNetEvent("dogFoundDrug") --Appelé par le serveur si de la drogue est touver a l'appel de l'event sera searchdrug(l'anim ne fonctionne pas)
AddEventHandler("dogFoundDrug", function()
	DisplayHelpText("drogue trouvé")
	RequestAnimDict("world_dog_barking@idle_a")
    while (not HasAnimDictLoaded("world_dog_barking@idle_a")) do Citizen.Wait(0) end
     TaskPlayAnim(dog,"world_dog_barking@idle_a", 1.0,-1.0,10000,0,1,true,true,true)
end)

AddEventHandler("stopfollowdogydogey", function() 
	ClearPedTasksImmediately(dog)
end)

AddEventHandler("deletedogydogey", function()
	if dog ~= nil then
		SetEntityAsMissionEntity(dog, true, true)
        DeleteEntity(dog)
		dog = nil
	end
end)

AddEventHandler("followdogydogey", function() --la boucle permet de réactivé le follow au cas ou le chien décide de ne plus suivre sont maitre (uniquement si le maitre est proche)
	Citizen.CreateThread(function()
		while following do
			Wait(0)
			if not following then
				break 
			end
			plypos = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, -2.0, 0.0)
			local dogCoord = GetOffsetFromEntityInWorldCoords(dog, 0.0, -2.0, 0.0, true)
			TaskFollowToOffsetOfEntity(dog, GetPlayerPed(PlayerId()), 0.2, 0.0, 0.0, -1, 10.0, 20, true)
			SetEntityMaxSpeed(dog,5000.0)
			--SetEntityProofs(dog, false, false, false, false, false, false, false, false)
		end
	end)
end)

AddEventHandler("foodLevel", function() -- niveau de nourriture du chien (pas encore test/utiliser)
	Citizen.CreateThread(function()
		while DoesEntityExist(dog) do
			Citizen.Wait(15000)
			if food > 0 then
				food = food -1
			else
				SetEntityHealth(dog, 0)
			end
		end
	end)
end)

AddEventHandler("setdogydogeyinvehicle", function(v) --faire entrer le chien dans un vehicule peu importe la distance(si les 2 places arriere sont utilisé le chien ce met devant)
	local booltest = true
	while boolbreak do
	Wait(0)
		if GetDistanceBetweenCoords(GetEntityCoords(v), GetEntityCoords(dog), true) < 2 then
			 	if inVehicle == false then
			 		following = false
			 		TriggerEvent("stopfollowdogydogey")
				 	if IsVehicleSeatFree(v, 1) then
				 		SetPedIntoVehicle(dog, v, 1)
				 	--	DisplayHelpText("place 1")
				 		inVehicle = true
				 	elseif IsVehicleSeatFree(v, 2) then
				 		SetPedIntoVehicle(dog, v, 2)
				 	--	DisplayHelpText("place 2")
				 		inVehicle = true
				 	elseif IsVehicleSeatFree(v, 0) then
				 		SetPedIntoVehicle(dog, v, 0)
				 	--	DisplayHelpText("place 0")
				 		inVehicle = true
				 	else
				 		DisplayHelpText("Aucune place n'est disponible")
				 	end
			 	end
			 if inVehicle then
				break 
			 end
	    elseif GetDistanceBetweenCoords(GetEntityCoords(v), GetEntityCoords(dog), true) > 2 and booltest then
		 	following = false
		 	TriggerEvent("stopfollowdogydogey")
		 	TaskEnterVehicle(dog, v, -1, 0, 2.0, 1, 0)
		 	booltest = false
		 	--DisplayHelpText("Le chien est trop loin de du véhicule")
		end
	end
end)
AddEventHandler("setdogydogeyoutvehicle", function(v) --sortir le chien d'un vehicle
--	DisplayHelpText("sortie")
	SetEntityProofs(dog, false, false, false, true, false, false, false, false)
	TaskLeaveVehicle(dog, v, 16)
	inVehicle = false
end)
AddEventHandler("searchdrug", function() --recherche de drogue sur le joueur le proche du chien(max 5m)
	local coords = GetEntityCoords(dog, true)
	local target, closestDistance = ESX.Game.GetClosestPlayer({x = coords.x, y = coords.y, z = coords.z})
	if target ~= nil and target ~= lastTarget and target ~= GetPlayerPed(-1) then
		local targetServerId = (GetPlayerServerId(target))
		local sourceServerId = (GetPlayerServerId(PlayerId()))
		if targetServerId ~= nil and closestDistance < 5.0 then
				TriggerServerEvent('esx:dogCheckInv', targetServerId,sourceServerId)
		end	
		targetServerId = nil
		lastTarget = target
	end
end)

--main
Citizen.CreateThread(function()
	while true do
		Wait(0)
		
		if  IsEntityDead(dog) or not DoesEntityExist(dog)then
			dog = nil
		end
		if IsControlJustPressed(1,38) and dog ~= nil then -- activer la fouille
			if search then
				search = false
			else
				search = true
			end
		end

		if search --[[ and PlayerData.job.name == 'police']] then -- commenté pour l'instant car sinon on ne peu plus restart juste le script mais ca devrais fonctionné
			TriggerEvent("searchdrug")
		end

		local plyCoord = GetEntityCoords(GetPlayerPed(-1))
		if  GetDistanceBetweenCoords(plyCoord, 460.65, -990.68, 30.45, true) < 1 then
			DisplayHelpText("appuyez sur ~INPUT_CONTEXT~ pour sortir dogydogey")
			if IsControlJustPressed(1,38) then
				TriggerEvent("spawndogydogey")
			end
		end
		if IsControlJustPressed(1,38) and dog ~= nil and GetDistanceBetweenCoords((GetEntityCoords(dog)),GetEntityCoords(GetPlayerPed(-1))) < 10 then
			if not following then
				following = true
				TriggerEvent("followdogydogey")
			--	DisplayHelpText("Follow")
			else
				following = false
				TriggerEvent("stopfollowdogydogey")
			--	DisplayHelpText("UnFollow")
			end
		end
		if IsControlJustPressed(1,245) and dog ~= nil then
			local inFrontOfPlayer = GetOffsetFromEntityInWorldCoords( GetPlayerPed(-1), 0.0, 10.000, 0.0 )
			local v = GetVehicleInDirection( GetEntityCoords(GetPlayerPed(-1),1), inFrontOfPlayer )
			if DoesEntityExist(v) then
				if inVehicle == false then
					boolbreak = true
					TriggerEvent("setdogydogeyinvehicle",v)
				elseif inVehicle == true then
					boolbreak = false
					TriggerEvent("setdogydogeyoutvehicle",v)
				end
			else
				DisplayHelpText("Aucun véhicle proche de vous")
			end
		end
	end
end)


--function

function DisplayHelpText(str)
  SetTextComponentFormat("STRING")
  AddTextComponentString(str)
  DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end
function GetVehicleInDirection( coordFrom, coordTo )
    local rayHandle = CastRayPointToPoint( coordFrom.x, coordFrom.y, coordFrom.z, coordTo.x, coordTo.y, coordTo.z, 10, GetPlayerPed( -1 ), 0 )
    local _, _, _, _, vehicle = GetRaycastResult( rayHandle )
    return vehicle
end

function notification(message)
	SetNotificationTextEntry("STRING")
	AddTextComponentString(message)
	DrawNotification(0,1)
end
