function lerp(x1, y1, z1, x2, y2, z2, t) 
      local x = x1 + (x2 - x1) * t
      local y = y1 + (y2 - y1) * t
      local z = z1 + (z2 - z1) * t
     return x, y, z
end

function onRender()
	--if IsControlPressed(1, 38) then
		aiming, ent = GetEntityPlayerIsFreeAimingAt(PlayerId())
		if aiming and ent and IsEntityAPed(ent) then
			health=GetEntityHealth(ent) or 0.0
			maxHealth=GetEntityMaxHealth(ent)-100 or 0.0
			val=(health/2)/maxHealth
			if IsPedDeadOrDying(ent, 1) then
				r,g,b=0,0,0
			else
				r,g,b=lerp(255,0,0,0,255,0,val)
			end
			--r,g,b=lerp(255,0,0,0,255,0,0.5)

			x1,y1,z1 = table.unpack(GetWorldPositionOfEntityBone(ent, i))

			DrawMarker(0,x1,y1,z1+1.0, 0.0, 0.0, 0.0, 0, 0, 0, 0.3, 0.0, -0.1, r,g,b , 50, false, true, false, false)

			DrawDebugText("Test",x1,y1,z1, r,g,b , 50)
		end
	--end
end

Citizen.CreateThread(function()
	while true do
		Wait(1)
		onRender()
	end
end)