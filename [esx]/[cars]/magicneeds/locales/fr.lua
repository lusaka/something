Locales['fr'] = {
	
	['used_bread'] = 'Vous avez mangé du ~b~pain',
	['used_fish'] = 'Vous avez mangé un ~b~poisson',
	['used_tacos'] = 'Vous avez mangé un ~b~tacos',
	['used_salad'] = 'Vous avez mangé une ~b~salade',
	['used_croissant'] = 'Vous avez mangé un ~b~croissant',
	['used_pizza'] = 'Vous avez mangé une ~b~part de pizza',
	['used_hamburger'] = 'Vous avez mangé un ~b~hamburger',
	['used_packaged_chicken'] = "Vous avez mangé du ~b~poulet",

	['used_water'] = "Vous avez bu de l'~b~eau",
	['used_tea'] = "Vous avez bu du ~b~thé",
	['used_cola'] = "Vous avez bu du ~b~coca",
	['used_coffee'] = "Vous avez bu du ~b~café",
	['used_beer'] = "Vous avez bu une ~b~bière",
	['used_triple_biffle'] = 'Vous avez bu une ~b~Triple Biffle',
	
	['used_holemeister'] = 'Vous avez bu une ~b~HoleMeister',
	['used_non_alcoholic_beer'] = 'Vous avez bu une ~b~FreshBeer',

	['used_steroid'] = 'Vous avez pris un ~b~stéroïde',

	['used_hair_oil'] = 'Vous avez les cheveux très ~y~huileux~s~. Je suppose que c\'est ~b~volontaire~s~ ?',
	['used_champagne'] = 'Vous avez bu une coupe de ... ah non ... vous avez siroté la bouteille de ~b~champagne~s~...',
	
	['used_clope'] = 'Vous fumez une clope',
}
