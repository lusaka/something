-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 29 mars 2018 à 20:01
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `essentialmode2`
--

-- --------------------------------------------------------

--
-- Structure de la table `position_drugs`
--

DROP TABLE IF EXISTS `position_drugs`;
CREATE TABLE IF NOT EXISTS `position_drugs` (
  `identifier` varchar(255) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `x` double(8,2) NOT NULL,
  `y` double(8,2) NOT NULL,
  `z` double(8,2) NOT NULL,
  `percent` varchar(50) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `position_drugs`
--

INSERT INTO `position_drugs` (`identifier`, `name`, `x`, `y`, `z`, `percent`) VALUES
('Juhel Jakez', 'Weed', -1192.77, -3051.98, 13.95, '0'),
('Juhel Jakez', 'Weed', -1191.68, -3046.80, 13.94, '0'),
('Juhel Jakez', 'Weed', -1179.27, -3029.44, 13.94, '0'),
('Juhel Jakez', 'Weed', -1176.21, -3031.14, 13.94, '0'),
('Juhel Jakez', 'Weed', -1171.66, -3033.74, 13.94, '0'),
('Juhel Jakez', 'Weed', -1167.68, -3036.00, 13.94, '0');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
