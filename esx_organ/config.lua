Config              = {}
Config.MarkerType   = 1
Config.DrawDistance = 100.0
Config.ZoneSize     = {x = 4.0, y = 4.0, z = 3.0}
Config.MarkerColor  = {r = 100, g = 204, b = 100}
Config.RequiredCopsCerveau = 0
Config.RequiredCopsCoeur = 0
Config.RequiredCopsMoelle = 0
Config.RequiredCopsIntestin = 0
Config.RequiredCopsBody = 0
Config.Locale = 'fr'

Config.Zones = {
	
    OsFarm =          {x=-1065.063,  y=-2879.711, z=12.988},
    OrganTreatment =  {x=-1055.770, y=-2882.595, z=12.964},
    BodyResell =      {x=-1053.915, y=-2888.616,  z=12.953}
}
