-------------------------------------------------
--  SCRIPT MADE BY MADRIAX FOR GTARPFRANCE     --
--		  https://discord.gg/fyytmPQ		   --
-------------------------------------------------

---------------------------
------CHOSE SQL MODE-------
---------------------------
--Async   -----------------
--MySQL   -----------------
--Couchdb ----------------- (soon)
---------------------------
local mode = "Async"

-- if (mode == "Async") then
--   require "resources/mysql-async/lib/MySQL"
-- elseif mode == "MySQL" then
--   --require "resources/[essential]/essentialmode/lib/MySQL" IF YOU'RE USING LALIFE SCRIPT !
--   require "resources/essentialmode/lib/MySQL"

  --DO NOT FORGET TO CHANGE THE LINE BELLOW !!!
--   MySQL:open("localhost", "gta5_eden", "root", "root")
-- end


local lang = 'fr'
local txt = {
  	['fr'] = {
        ['welcome'] = 'Bienvenue dans votre appartement!\n',
        ['nocash'] = 'Vous n\'avez pas assez d\'argent!\n',
        ['estVendu'] = 'Appartement vendus!\n'
  },

    ['en'] = {
        ['welcome'] = 'Welcome to home!\n',
        ['nocash'] = 'You d\'ont have enough cash!\n',
        ['estVendu'] = 'Apartment sold!\n'
    }
}


local isBuy = 0
local money = 0
local dirtymoney = 0

-- RegisterServerEvent("apart:getAppart")
-- AddEventHandler('apart:getAppart', function(name)
--   TriggerEvent('es:getPlayerFromId', source, function(user)
--     local player = user.getIdentifier()
--     local name = name
--     if (mode == "Async") then
--       MySQL.Async.fetchAll("SELECT * FROM user_appartement WHERE name = @nom", {['@nom'] = tostring(name)}, function (result)
--         if (result) then
--           count = 0
--           for _ in pairs(result) do
--             count = count + 1
--           end
--           if count > 0 then
--           	if (result[1].identifier == player) then
--               print("IS MINE SERVER")
--           		TriggerClientEvent('apart:isMine', source)
--           	else
--               	TriggerClientEvent('apart:isBuy', source)
--           	end
--           else
--           	TriggerClientEvent('apart:isNotBuy', source)
--           end
--         end
--       end)
--     end
--   end)
-- end)

RegisterServerEvent("apart:getAppart")
AddEventHandler('apart:getAppart', function(name)
    local sourcePlayer = tonumber(source)
    local player = getPlayerID(sourcePlayer)
    local name = name
    if (mode == "Async") then
      MySQL.Async.fetchAll("SELECT * FROM user_appartement WHERE name = @nom", {['@nom'] = tostring(name)}, function (result)
        if (result) then
          count = 0
          for _ in pairs(result) do
            count = count + 1
          end
          if count > 0 then
            if (result[1].identifier == player) then
              print("IS MINE SERVER")
              TriggerClientEvent('apart:isMine', sourcePlayer)
            else
                TriggerClientEvent('apart:isBuy', sourcePlayer)
            end
          else
            TriggerClientEvent('apart:isNotBuy', sourcePlayer)
          end
        end
      end)
    end
end)

RegisterServerEvent("apart:getCash")
AddEventHandler('apart:getCash', function(name)
  TriggerEvent('es:getPlayerFromId', source, function(user)
    local player = user.getIdentifier()
    local name = name
    if (mode == "Async") then
      MySQL.Async.fetchAll("SELECT * FROM user_appartement WHERE name = @nom", {['@nom'] = tostring(name)}, function (result)
        if (result) then
          money = result[1].money
          dirtymoney = result[1].dirtymoney
          TriggerClientEvent('apart:getCash', source, money, dirtymoney)
        end
      end)
    elseif mode == "MySQL" then
      local executed_query = MySQL:executeQuery("SELECT * FROM user_appartement WHERE name = @nom", {['@nom'] = tostring(name)})
      local result = MySQL:getResults(executed_query, {'identifier'})
      if (result) then
        money = result[1].money
        dirtymoney = result[1].dirtymoney
        TriggerClientEvent('apart:getCash', source, money, dirtymoney)
      end
    end
  end)
end)

RegisterServerEvent("apart:depositcash")
AddEventHandler('apart:depositcash', function(cash, apart)
  TriggerEvent('es:getPlayerFromId', source, function(user)
    local player = user.getIdentifier()
    local money = 0
    if (tonumber(user.money) >= tonumber(cash) and tonumber(cash) > 0) then
      if mode == "Async" then
        MySQL.Async.fetchAll("SELECT money FROM user_appartement WHERE name = @nom", {['@nom'] = apart}, function (result)
            if (result) then
              money = result[1].money
              user:removeMoney((cash))
              local newmoney = money + cash
              MySQL.Async.execute("UPDATE user_appartement SET `money`=@cash WHERE name = @nom",{['@cash'] = newmoney, ['@nom'] = apart}, function(data)
              end)
            end
        end)
      elseif mode == "MySQL" then
        local executed_query = MySQL:executeQuery("SELECT money FROM user_appartement WHERE name = @nom", {['@nom'] = apart})
        local result = MySQL:getResults(executed_query, {'money'})
        if (result) then
          money = result[1].money
          user:removeMoney((cash))
          local newmoney = money + cash
          MySQL:executeQuery("UPDATE user_appartement SET `money`=@cash WHERE name = @nom",{['@cash'] = newmoney, ['@nom'] = apart})
        end
      end

      TriggerClientEvent('apart:getCash', source, money, dirtymoney)
    else
      TriggerClientEvent("es_freeroam:notify", source, "CHAR_SIMEON", 1, "Stephane", false, txt[lang]['nocash']) --(FOR FREEROAM)
      --TriggerClientEvent("citizenv:notify", source, "CHAR_SIMEON", 1, "Stephane", false, txt[lang]['nocash']) --WITH LALIFE SCRIPTS
    end
  end)
end)

RegisterServerEvent("apart:depositdirtycash")
AddEventHandler('apart:depositdirtycash', function(cash, apart)
  TriggerEvent('es:getPlayerFromId', source, function(user)
    local player = user.getIdentifier()
    local money = 0
    if (tonumber(user:getDMoney()) >= tonumber(cash) and tonumber(cash) > 0) then
      if mode == "Async" then
        MySQL.Async.fetchAll("SELECT dirtymoney FROM user_appartement WHERE name = @nom", {['@nom'] = apart}, function (result)
            if (result) then
              money = result[1].dirtymoney
              user:removeDMoney((cash))
              local newmoney = money + cash
              MySQL.Async.execute("UPDATE user_appartement SET `dirtymoney`=@cash WHERE name = @nom",{['@cash'] = newmoney, ['@nom'] = apart}, function(data)
              end)
            end
        end)
      elseif mode == "MySQL" then
        local executed_query = MySQL:executeQuery("SELECT dirtymoney FROM user_appartement WHERE name = @nom", {['@nom'] = apart})
        local result = MySQL:getResults(executed_query, {'dirtymoney'})
        if (result) then
          money = result[1].dirtymoney
          user:removeDMoney((cash))
          local newmoney = money + cash
          MySQL:executeQuery("UPDATE user_appartement SET `dirtymoney`=@cash WHERE name = @nom",{['@cash'] = newmoney, ['@nom'] = apart})
        end
      end

      TriggerClientEvent('apart:getCash', source, money, dirtymoney)
    else
      TriggerClientEvent("es_freeroam:notify", source, "CHAR_SIMEON", 1, "Stephane", false, txt[lang]['nocash']) --(FOR FREEROAM)
      -- TriggerClientEvent("citizenv:notify", source, "CHAR_SIMEON", 1, "Stephane", false, txt[lang]['nocash']) --WITH LALIFE SCRIPTS
    end
  end)
end)

RegisterServerEvent("apart:takecash")
AddEventHandler('apart:takecash', function(cash, apart)
  TriggerEvent('es:getPlayerFromId', source, function(user)
    local player = user.getIdentifier()
    local money = 0
    if mode == "Async" then
      MySQL.Async.fetchAll("SELECT money FROM user_appartement WHERE name = @nom", {['@nom'] = apart}, function (result)
          if (result) then
            money = result[1].money
            if (tonumber(cash) <= tonumber(money) and tonumber(cash) > 0) then
              user:addMoney((cash))
              local newmoney = money - cash
              MySQL.Async.execute("UPDATE user_appartement SET `money`=@cash WHERE name = @nom",{['@cash'] = newmoney, ['@nom'] = apart}, function(data)
              end)
            else
              TriggerClientEvent("es_freeroam:notify", source, "CHAR_SIMEON", 1, "Stephane", false, txt[lang]['nocash']) --(FOR FREEROAM)
              -- TriggerClientEvent("citizenv:notify", source, "CHAR_SIMEON", 1, "Stephane", false, txt[lang]['nocash']) --WITH LALIFE SCRIPTS
            end
          end
      end)
    elseif mode == "MySQL" then
      local executed_query = MySQL:executeQuery("SELECT money FROM user_appartement WHERE name = @nom", {['@nom'] = apart})
      local result = MySQL:getResults(executed_query, {'money'})
      if (result) then
        money = result[1].money
        if (tonumber(cash) <= tonumber(money) and tonumber(cash) > 0) then
          user:addMoney((cash))
          local newmoney = money - cash
          MySQL:executeQuery("UPDATE user_appartement SET `money`=@cash WHERE name = @nom",{['@cash'] = newmoney, ['@nom'] = apart})
        else
          TriggerClientEvent("es_freeroam:notify", source, "CHAR_SIMEON", 1, "Stephane", false, txt[lang]['nocash']) --(FOR FREEROAM)
          --TriggerClientEvent("citizenv:notify", source, "CHAR_SIMEON", 1, "Stephane", false, txt[lang]['nocash']) --WITH LALIFE SCRIPTS
        end
      end
    end

    TriggerClientEvent('apart:getCash', source, money, dirtymoney)
  end)
end)

RegisterServerEvent("apart:takedirtycash")
AddEventHandler('apart:takedirtycash', function(cash, apart)
  TriggerEvent('es:getPlayerFromId', source, function(user)
    local player = user.getIdentifier()
    local money = 0
    if mode == "Async" then
      MySQL.Async.fetchAll("SELECT dirtymoney FROM user_appartement WHERE name = @nom", {['@nom'] = apart}, function (result)
          if (result) then
            money = result[1].money
            if (tonumber(cash) <= tonumber(money) and tonumber(cash) > 0) then
              user:addDMoney((cash))
              local newmoney = money - cash
              MySQL.Async.execute("UPDATE user_appartement SET `dirtymoney`=@cash WHERE name = @nom",{['@cash'] = newmoney, ['@nom'] = apart}, function(data)
              end)
            else
              TriggerClientEvent("es_freeroam:notify", source, "CHAR_SIMEON", 1, "Stephane", false, txt[lang]['nocash']) --(FOR FREEROAM)
              -- TriggerClientEvent("citizenv:notify", source, "CHAR_SIMEON", 1, "Stephane", false, txt[lang]['nocash']) --WITH LALIFE SCRIPTS
            end
          end
      end)
    elseif mode == "MySQL" then
      local executed_query = MySQL:executeQuery("SELECT dirtymoney FROM user_appartement WHERE name = @nom", {['@nom'] = apart})
      local result = MySQL:getResults(executed_query, {'dirtymoney'})
      if (result) then
        money = result[1].money
        if (tonumber(cash) <= tonumber(money) and tonumber(cash) > 0) then
          user:addDMoney((cash))
          local newmoney = money - cash
          MySQL:executeQuery("UPDATE user_appartement SET `dirtymoney`=@cash WHERE name = @nom",{['@cash'] = newmoney, ['@nom'] = apart})
        else
          TriggerClientEvent("es_freeroam:notify", source, "CHAR_SIMEON", 1, "Stephane", false, txt[lang]['nocash']) --(FOR FREEROAM)
          -- TriggerClientEvent("citizenv:notify", source, "CHAR_SIMEON", 1, "Stephane", false, txt[lang]['nocash']) --WITH LALIFE SCRIPTS
        end
      end
    end

    TriggerClientEvent('apart:getCash', source, money, dirtymoney)
  end)
end)

RegisterServerEvent("apart:buyAppart")
AddEventHandler('apart:buyAppart', function(name, price)
  TriggerEvent('es:getPlayerFromId', source, function(user)    
    local player = user.getIdentifier()
    local name = name
    local price = price
    
    if (tonumber(user.getMoney()) >= tonumber(price)) then
        user.removeMoney((price))
      if (mode == "Async") then
        print("buyAppart")
    	  MySQL.Async.execute("INSERT INTO user_appartement (`identifier`, `name`, `price`) VALUES (@username, @name, @price)", {['@username'] = player, ['@name'] = name, ['@price'] = price})      
      end
      --TriggerClientEvent("es_freeroam:notify", source, "CHAR_SIMEON", 1, "Stephane", false, txt[lang]['welcome']) --(FOR FREEROAM)
    	-- TriggerClientEvent("citizenv:notify", source, "CHAR_SIMEON", 1, "Stephane", false, txt[lang]['welcome']) --WITH LALIFE SCRIPTS
    	TriggerClientEvent('apart:isMine', source)
    --else
    	-- TriggerClientEvent("es_freeroam:notify", source, "CHAR_SIMEON", 1, "Stephane", false, txt[lang]['nocash']) --(FOR FREEROAM)
    	-- TriggerClientEvent("citizenv:notify", source, "CHAR_SIMEON", 1, "Stephane", false, txt[lang]['nocash']) --WITH LALIFE SCRIPTS
    end
  end)
end)

RegisterServerEvent("apart:sellAppart")
AddEventHandler('apart:sellAppart', function(name, price)
  TriggerEvent('es:getPlayerFromId', source, function(user)
    local player = user.getIdentifier()
    local name = name
    local price = price/2
    user:addMoney((price))
      if (mode == "Async") then
        MySQL.Async.execute("DELETE from user_appartement WHERE identifier = @username AND name = @name",
        {['@username'] = player, ['@name'] = name})
      elseif mode == "MySQL" then
        local executed_query3 = MySQL:executeQuery("DELETE from user_appartement WHERE identifier = @username AND name = @name",
        {['@username'] = player, ['@name'] = name})
      end
      TriggerClientEvent("es_freeroam:notify", source, "CHAR_SIMEON", 1, "Stephane", false, txt[lang]['estVendu']) --(FOR FREEROAM)
      -- TriggerClientEvent("citizenv:notify", source, "CHAR_SIMEON", 1, "Stephane", false, txt[lang]['estVendu']) --WITH LALIFE SCRIPTS
      TriggerClientEvent('apart:isNotBuy', source)
  end)
end)

RegisterServerEvent("wardrobe:getOutfit")
AddEventHandler('wardrobe:getOutfit', function(outfit)
  TriggerEvent('es:getPlayerFromId', source, function(user)
    local player = user.getIdentifier()
    local outfit = outfit
    if (mode == "Async") then
      MySQL.Async.fetchAll("SELECT * FROM user_wardrobes WHERE identifier = @username AND outfit = @outfit", {['@username'] = player, ['@outfit'] = tonumber(outfit)}, function (result)
        if (result) then
          count = 0
          for _ in pairs(result) do
            count = count + 1
          end
          if count > 0 then
            if (result[1].identifier == player) then
              TriggerClientEvent("wardrobe:loadOutfit_client", source, result[1])
            end
          end
        end
      end)
    end
  end)
end)

RegisterServerEvent("wardrobe:saveOutfit")
AddEventHandler('wardrobe:saveOutfit', function(outfitNumber, outfit_drawables, outfit_textures)
  TriggerEvent('es:getPlayerFromId', source, function(user)
    local player = user.getIdentifier()
    local outfitNumber = outfitNumber
    local outfit_drawables = outfit_drawables
    local outfit_textures = outfit_textures

    if mode == "Async" then
      MySQL.Async.fetchAll("SELECT * FROM user_wardrobes WHERE identifier = @username AND outfit = @outfit", {['@username'] = player, ['@outfit'] = tonumber(outfitNumber)}, 
        function (result)
          count = 0
          for _ in pairs(result) do
            count = count + 1
          end
          if count > 0 then
            print("UPDATE")
            MySQL.Async.execute("UPDATE user_wardrobes SET outfit = @outfit, shirt = @shirt, pants = @pants, shoes = @shoes, vest = @vest, bag = @bag, gloves = @gloves,  jacket = @jacket, shirt_texture = @shirt_texture, pants_texture = @pants_texture, shoes_texture = @shoes_texture, vest_texture = @vest_texture, bag_texture = @bag_texture, gloves_texture = @gloves_texture,  jacket_texture = @jacket_texture WHERE identifier = @username",
              {['@username'] = player, ['@outfit'] = tonumber(outfitNumber),
               ['@shirt'] = outfit_drawables["shirt"], ['@pants'] = outfit_drawables["pants"],['@shoes'] = outfit_drawables["shoes"],['@vest'] = outfit_drawables["vest"],
               ['@bag'] = outfit_drawables["bag"],['@gloves'] = outfit_drawables["gloves"],['@jacket'] = outfit_drawables["jacket"],               
               ['@shirt_texture'] = outfit_textures["shirt"], ['@pants_texture'] = outfit_textures["pants"],['@shoes_texture'] = outfit_textures["shoes"],
               ['@vest_texture'] = outfit_textures["vest"],['@bag_texture'] = outfit_textures["bag"],['@gloves_texture'] = outfit_textures["gloves"],
               ['@jacket_texture'] = outfit_textures["jacket"]
              }
              )
            --MySQL.Async.execute("INSERT INTO user_wardrobes SELECT * FROM user_clothes WHERE identifier = @username"
          else
            print("INSERT")
            MySQL.Async.execute("INSERT INTO user_wardrobes ( identifier, outfit, shirt, pants, shoes, vest, bag, gloves, jacket, shirt_texture, pants_texture, shoes_texture, vest_texture, bag_texture, gloves_texture, jacket_texture) VALUES (@username, @outfit, @shirt, @pants, @shoes, @vest, @bag, @gloves, @jacket, @shirt_texture, @pants_texture, @shoes_texture, @vest_texture, @bag_texture, @gloves_texture, @jacket_texture)",
              {['@username'] = player, ['@outfit'] = tonumber(outfitNumber),
               ['@shirt'] = outfit_drawables["shirt"], ['@pants'] = outfit_drawables["pants"],['@shoes'] = outfit_drawables["shoes"],['@vest'] = outfit_drawables["vest"],
               ['@bag'] = outfit_drawables["bag"],['@gloves'] = outfit_drawables["gloves"],['@jacket'] = outfit_drawables["jacket"],               
               ['@shirt_texture'] = outfit_textures["shirt"], ['@pants_texture'] = outfit_textures["pants"],['@shoes_texture'] = outfit_textures["shoes"],
               ['@vest_texture'] = outfit_textures["vest"],['@bag_texture'] = outfit_textures["bag"],['@gloves_texture'] = outfit_textures["gloves"],
               ['@jacket_texture'] = outfit_textures["jacket"]
              }
              )
          end
        end
      )
    end
  end)
end)

function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end